import sys
import ast
import torch
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import matplotlib.pyplot as plt
import time

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]
GateId = int(args[22])
Net = args[23]

AllGates = ['NOT','AND','OR','XOR','Multi','PatternReg']
Gate = AllGates[GateId]

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)

if ((Gate=='AND') | (Gate=='OR') | (Gate=='XOR')):
	Input_1 = [low,low]
	Input_2 = [low,high]
	Input_3 = [high,low]
	Input_4 = [high,high]
	Input_5 = [mid,mid]
	# Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5]
	Inputs = [Input_1,Input_2,Input_3,Input_4]
	# Inputs = [Input_4,Input_1,Input_2,Input_3]
	if Gate=='AND':
		Output_1 = [low]
		Output_2 = [low]
		Output_3 = [low]
		Output_4 = [high]
		Output_5 = [mid]
	elif Gate=='OR':
		Output_1 = [low]
		Output_2 = [high]
		Output_3 = [high]
		Output_4 = [high]
	elif Gate=='XOR':
		Output_1 = [low]
		Output_2 = [high]
		Output_3 = [high]
		Output_4 = [low]
	Outputs = [Output_1,Output_2,Output_3,Output_4]
	# Outputs = [Output_4,Output_1,Output_2,Output_3]
	# Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5]
elif Gate=='NOT':
	Input_1 = [low]
	Input_2 = [high]
	Input_3 = [mid]
	# Input_2 = [mid+10]   # TESTING ONLY
	# Inputs = [Input_1,Input_2,Input_3]
	Inputs = [Input_1,Input_2]
	Output_1 = [high]
	Output_2 = [low]
	Output_3 = [mid]
	# Outputs = [Output_1,Output_2,Output_3]
	Outputs = [Output_1,Output_2]
elif Gate=='Multi':
	# OR
	Input_1 = [low,low,low]
	Input_2 = [low,high,low]
	Input_3 = [high,low,low]
	Input_4 = [high,high,low]
	# AND
	Input_5 = [low,low,high]
	Input_6 = [low,high,high]
	Input_7 = [high,low,high]
	Input_8 = [high,high,high]
	Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5,Input_6,Input_7,Input_8]
	# OR
	Output_1 = [low]
	Output_2 = [low]
	Output_3 = [low]
	Output_4 = [high]
	# AND
	Output_5 = [low]
	Output_6 = [high]
	Output_7 = [high]
	Output_8 = [high]
	Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5,Output_6,Output_7,Output_8]
elif Gate=='PatternReg':
	Num_samples = 2
	# def PerturbPattern(pattern,delta,group):
	# 	num_pixels = len(pattern)
	# 	if (group == 1):
	# 		delta_index_range = np.arange(-delta,1)
	# 	elif (group == 2):
	# 		delta_index_range = np.arange(0,delta+1)
	# 	perturbation = np.random.choice(delta_index_range,num_pixels)
	# 	new_indices = (pattern + perturbation)
	# 	return(list(new_indices))  # turn np array into a regular list
	# Main_pattern_1 = [high,high,high]
	# Main_pattern_2 = [low,low,low]
	def PerturbPattern(pattern,delta):
		num_pixels = len(pattern)
		delta_index_range = np.arange(-delta,delta+1)
		perturbation = np.random.choice(delta_index_range,num_pixels)
		pattern = (pattern + perturbation)
		pattern[pattern < 0] = 0
		pattern[pattern >= num_polarity_levels] = num_polarity_levels-1
		return(list(pattern))  # turn np array into a regular list
	Main_pattern_1 = [high,low]
	Main_pattern_2 = [high,high]
	Main_pattern_3 = [low,high]
	Main_pattern_4 = [low,low]
	Inputs = list([Main_pattern_1,Main_pattern_2,Main_pattern_3])  # Note that "Inputs" contains indices into "Polarity_levels", not the polarities themselves
	Outputs = list([Main_pattern_1,Main_pattern_2,Main_pattern_3])  # first item corresponds to the main pattern
	Num_samples_1 = Num_samples_2 = Num_samples_3 = Num_samples_4 = int(Num_samples/4)
	MaxPerturbSize = int(num_polarity_levels/2)-5
	for i in range(Num_samples_1 - 1):  # Note the main pattern, not included here, is a correct sample
		Inputs.append(PerturbPattern(Main_pattern_1,MaxPerturbSize))
		Outputs.append(Main_pattern_1)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
	for i in range(Num_samples_2 - 1):
		Inputs.append(PerturbPattern(Main_pattern_2,MaxPerturbSize))
		Outputs.append(Main_pattern_2)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
	for i in range(Num_samples_3 - 1):
		Inputs.append(PerturbPattern(Main_pattern_3,MaxPerturbSize))
		Outputs.append(Main_pattern_3)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
	for i in range(Num_samples_4 - 1):
		Inputs.append(PerturbPattern(Main_pattern_4,MaxPerturbSize))
		Outputs.append(Main_pattern_4)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list

InputCells = list(range(np.prod(Layering[0])))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))
# OutputCells = InputCells.copy()
# NumCells = int(np.sum([np.prod(x) for x in Layering]))
# OutputCells.extend(list(range(NumCells-1,NumCells)))

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA, or when
LoadNetwork = True  # should be True for both simulation and learning
NodeList = []
SequentialInputs = True
InputSequenceLength = 8
EvaluationProp = 0.1
individual = 0
StartTime = time.time()

if LoadNetwork:
	data = torch.load('./Data/'+SimParamFiles[0])
	if (len(data)==12):
		_, _, _, InitWeights, InitBias, InitW, BestWeights, BestBias, BestW, _, _, _ = data  # Newest format has length 12
	else:
		print('Wrong file format')
	NetworkParams = [BestW.clone(),BestBias.clone()]
else:
	NetworkParams = []

TurnOffGapJuncGating = False
TurnOffSignalNonlinearity = False
BlockNodes = []
LearnNodeList = []
LearnEdgeList = []
BlockNodes = []

arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
			 NumSimIters,SimParamFiles,NodeList,\
			 LearnNodeList,LearnEdgeList,\
			 Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
			 dgj_scale,vm_scale,time_step,EvaluationProp,\
			 SaveFile,SaveFileVersion,JobId,\
			 RecordVisualData,\
			 Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
			 individual,StartTime,WallTime,\
			 BlockNodes]

simBEN = SimBEN(arguments)

simBEN.MasterLearn()
# print(simBEN.ben.vm_time.shape,simBEN.ben.Dgj_time.shape,simBEN.ben.cc_sig_time.shape)
# print(simBEN.ben.vm,simBEN.ben.TotalError)

print(simBEN.ben.MeanError)

vm_time = simBEN.ben.vm_time_all
cc_sig_time = simBEN.ben.cc_sig_time_all
W_time = simBEN.ben.Dgj_time
NumEdges = simBEN.ben.NumEdges
EdgeList = np.array(torch.nonzero(torch.triu(simBEN.ben.BaseAdj)))
n = simBEN.ben.n
NumInputs = vm_time.shape[1]

state_time = vm_time*1000
# state_time = cc_sig_time

Plot = True

# np.save('./Data/TimeSeriesVmemAND.dat',state_time)

if Plot:
	if ((Gate=='AND') or (Gate=='XOR')):  # AND: [[2,1],[6,1],[1,1]]; XOR: [[2,1],[2,1],[1,1]]
		# Plot vm time series of all cells
		fig, ax = plt.subplots(1,1,figsize=(16,10))
		PlotIndices = [3]  # Plot only the first sequence of a set of sequences
		for i in PlotIndices:
		# for i in [0]:
			if len(PlotIndices)==1:
				AX = ax
			else:
				AX = ax.flat[i]
			x = np.asarray(state_time[:,i,:].data)
			# for j in range(2):
			(AX).plot(x[:,0],color='blue') #,linewidth=6)
			(AX).plot(x[:,1],color='red') #,linewidth=6)
			# for j in range(2,4):
			# 	(AX).plot(x[:,j],color='grey')
			# (AX).plot(x[:,2],color='grey')
			# (AX).plot(x[:,3],color='grey')
			# (AX).plot(x[:,4],color='grey')
			# (AX).plot(x[:,5],color='grey')
			# (AX).plot(x[:,6],color='grey')
			# (AX).plot(x[:,7],color='grey')
			# (ax.flat[i]).plot(x[0:150,2],color='grey')
			# (ax.flat[i]).plot(x[0:150,6],color='grey')
			# (ax.flat[i]).plot(x[0:150,3],color='black')
			# (ax.flat[i]).plot(x[0:150,4],color='black')
			# (ax.flat[2]).plot(x[:,3],color='grey')
			# (ax.flat[i]).plot(x[0:150,8],color='blue')
			# (ax.flat[3]).plot(x[:,4],color='grey')
			# (ax.flat[3]).plot(x[:,8],color='blue')
			# (ax.flat[0]).plot(x[:,2],color='grey')
			# (ax.flat[0]).plot(x[:,8],color='blue')
			# (ax.flat[1]).plot(x[:,7],color='grey')
			# (ax.flat[1]).plot(x[:,8],color='blue')
			# (ax.flat[2]).plot(x[:,3],color='grey')
			# (ax.flat[2]).plot(x[:,8],color='blue')
			# (ax.flat[3]).plot(x[:,4],color='grey')
			# (ax.flat[3]).plot(x[:,8],color='blue')
			# (ax.flat[i]).plot(x[:,2],color='red')
			# (ax.flat[i]).plot(x[:,5],color='green')
			(AX).plot(x[:,-1],color='green') #,linewidth=6)
			(AX).set_ylim(-100,100)  # for Vmem
			# (ax.flat[i]).set_ylim(-0.01,1.01)  # for cc_sig
			# if ((i==3) or (i==4)):
			(AX).set_xlabel('Time',fontsize=20)  # 30 for ppt
			# if ((i == 0) or (i==3)):
			(AX).set_ylabel('Vmem (mV)',fontsize=20)  # 30 for ppt
			AX.tick_params(axis='both',labelsize=16)  # 24 for ppt
		fig.tight_layout()
		plt.show()
		fname = './Figures/TimeSeriesVmem' + Gate + str(Net) + '.png'
		# fname = './Figures/TimeSeriesVmemLarge' + Gate + str(Net) + '.png'
		fig.savefig(fname,bbox_inches='tight')
	elif (Gate=='NOT'):  # NOT: [[1,1],[6,1],[1,1]]
		# Plot vm time series of all cells
		fig, ax = plt.subplots(2,2,figsize=(20,10))
		for i in range(NumInputs):
			x = np.asarray(state_time[:,i,:].data)
			for j in range(1):
				(ax.flat[i]).plot(x[:,j],color='red')
			# for j in range(1,7):
			# 	(ax.flat[i]).plot(x[:,j],color='grey')
			(ax.flat[i]).plot(x[:,7],color='blue')
			(ax.flat[i]).set_ylim(-100,100)  # for Vmem
			# (ax.flat[i]).set_ylim(-0.01,1.01)  # for cc_sig
			if ((i==2) or (i==3)):
				(ax.flat[i]).set_xlabel('Time',fontsize=20)
			if ((i == 0) or (i==2)):
				(ax.flat[i]).set_ylabel('Vmem (mV)',fontsize=20)
		fig.tight_layout()
		plt.show()
		# fname = './Figures/TimeSeriesVmem' + Gate + str(Net) + '.png'
		# fig.savefig(fname,bbox_inches='tight')
	elif (Gate=='Multi'):
		# Plot vm time series of all cells
		fig, ax = plt.subplots(1,1,figsize=(16,10))
		PlotIndices = [0]  # Plot only the first sequence of a set of sequences
		for i in PlotIndices:
			if len(PlotIndices)==1:
				AX = ax
			else:
				AX = ax.flat[i]
			x = np.asarray(state_time[:,i,:].data)
			(AX).plot(x[:,0],color='orange') #,linewidth=6)
			(AX).plot(x[:,1],color='purple') #,linewidth=6)
			(AX).plot(x[:,2],color='grey') #,linewidth=6)
			(AX).plot(x[:,39],color='green') #,linewidth=6)
			(AX).set_ylim(-100,100)  # for Vmem
			(AX).set_xlabel('Time',fontsize=20)  # 30 for ppt
			(AX).set_ylabel('Vmem (mV)',fontsize=20)  # 30 for ppt
			AX.tick_params(axis='both',labelsize=16)  # 24 for ppt
		fig.tight_layout()
		plt.show()
		# fname = './Figures/TimeSeriesVmem' + Gate + str(Net) + '.png'
		# fname = './Figures/TimeSeriesVmemLarge' + Gate + str(Net) + '.png'
		# fig.savefig(fname,bbox_inches='tight')
	elif (Gate=='PatternReg'):
		# Plot vm time series of all cells
		fig, ax = plt.subplots(1,1,figsize=(16,10))
		PlotIndices = [0]  # Plot only the first sequence of a set of sequences
		for i in PlotIndices:
			if len(PlotIndices)==1:
				AX = ax
			else:
				# AX = ax.flat[i]
				AX = ax
			x = np.asarray(state_time[:,i,:].data)
			(AX).plot(x[:,0],color='red') #,linewidth=6)
			(AX).plot(x[:,1],color='blue') #,linewidth=6)
			# (AX).plot(x[:,2],color='green') #,linewidth=6)
			(AX).set_ylim(-100,100)  # for Vmem
			(AX).set_xlabel('Time',fontsize=20)  # 30 for ppt
			(AX).set_ylabel('Vmem (mV)',fontsize=20)  # 30 for ppt
			AX.tick_params(axis='both',labelsize=16)  # 24 for ppt
		fig.tight_layout()
		plt.show()
		# fname = './Figures/TimeSeriesVmemPatternRegulator.png'
		# fig.savefig(fname,bbox_inches='tight')
