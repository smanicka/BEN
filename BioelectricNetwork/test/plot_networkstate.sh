#!/bin/bash
Gate=5
Net=0
walltime="0-01:00:00"
# BestParamFile=$(python FindBestParamFile.py $Gate $Net)
# echo $BestParamFile
# python PlotTimeSeries.py 1 [300] False False False [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateAND_0_320.dat LogicGateXXX.dat 999 999 True $Gate $Net
# python PlotTimeSeries.py 1 [300] False False False [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateXOR_0_229.dat LogicGateXXX.dat 999 999 True $Gate $Net
# python PlotTimeSeries.py 1 [500] False False False [[2,1],[5,5],[1,1]] True False [] False 0.0 0.0 1.0 1.0 0.5 LogicGateEvolveAND_36_999.dat LogicGateXXX.dat 999 999 True $Gate $Net
# python PlotTimeSeriesPatternDetector.py 1 [600] False False False [[3,6],[5,5],[1,1]] True False [] False 0.2 1.0 1.0 1.0 0.5 PatternDetectEvolveFrench_7_2.dat PatternDetectXXX.dat 999 999 True
# python PlotPatternDetectionPerformance.py 1 [600] False False False [[3,6],[5,5],[1,1]] True False [] False 0.2 1.0 1.0 1.0 0.5 PatternDetectEvolveFrench_7_2.dat PatternDetectXXX.dat 999 999 True
# python PlotTimeSeries.py 1 [500] False False False [[3,1],[6,6],[1,1]] True False [] False 0.1 1.0 1.0 1.0 0.5 LogicGateEvolveMulti_30_1.dat LogicGateXXX.dat 999 999 True $Gate $Net
# python PlotTimeSeries.py 1 [500] False False False [[2,1],[6,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 0.5 LogicGateAND_0_677.dat LogicGateXXX.dat 999 999 True $Gate $Net
#python -m BioelectricNetwork.analyze.PlotTimeSeries 1 [600] Simulate False False [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateEvolveANDSmall_55_1.dat LogicGateXXX.dat 999 999 True $walltime $Gate $Net
#python -m BioelectricNetwork.analyze.PlotTimeSeries 1 [600] Simulate False False [[2,1],[6,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 0.5 LogicGateAND_0_677.dat LogicGateXXX.dat 999 999 True $walltime $Gate $Net
#python -m BioelectricNetwork.analyze.PlotTimeSeries 1 [300] Simulate False False [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateAND_0_320.dat LogicGateXXX.dat 999 999 True $walltime $Gate $Net
#python -m BioelectricNetwork.analyze.PlotNetworkState 1 [3000] Simulate False False [[3,1]] True False [0] False 0.0 0.0 1.0 1.0 1.0 PatternRegulator_0_62.dat PatternRegulatorXXX.dat 999 999 True $walltime $Gate $Net
#python -m BioelectricNetwork.analyze.PlotNetworkState 1 [3000] Simulate False False [[2,1]] True False [0] False 0.0 0.0 1.0 1.0 1.0 PatternRegulator_0_230.dat PatternRegulatorXXX.dat 999 999 True $walltime $Gate $Net
python -m BioelectricNetwork.analyze.PlotNetworkState 1 [1000] Simulate False False [[2,1]] True False [0] False 0.0 0.0 1.0 1.0 1.0 PatternRegulator_0_415.dat PatternRegulatorXXX.dat 999 999 True $walltime $Gate $Net