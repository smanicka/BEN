import sys
import ast
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import time

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd 
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)

# def PerturbPattern(pattern,delta,group):
#     num_pixels = len(pattern)
#     if (group == 1):
#         delta_index_range = np.arange(-delta,1)
#     elif (group == 2):
#         delta_index_range = np.arange(0,delta+1)
#     perturbation = np.random.choice(delta_index_range,num_pixels)
#     new_indices = (pattern + perturbation)
#     return(list(new_indices))  # turn np array into a regular list

# Main_pattern_1 = [high,high,high]
# Main_pattern_2 = [low,low,low]

def PerturbPattern(pattern,delta):
    num_pixels = len(pattern)
    delta_index_range = np.arange(-delta,delta+1)
    perturbation = np.random.choice(delta_index_range,num_pixels)
    pattern = (pattern + perturbation)
    pattern[pattern < 0] = 0
    pattern[pattern >= num_polarity_levels] = num_polarity_levels-1
    return(list(pattern))  # turn np array into a regular list

# AttrLevelLow  = int((low+mid)/2)
# AttrLevelHigh = int((mid+high)/2)
AttrLevelLow  = low
AttrLevelHigh = high
Main_pattern_1 = [AttrLevelHigh,AttrLevelLow]
Main_pattern_2 = [AttrLevelHigh,AttrLevelHigh]

Num_samples = 40
Inputs = list([Main_pattern_1,Main_pattern_2])  # Note that "Inputs" contains indices into "Polarity_levels", not the polarities themselves
Outputs = list([Main_pattern_1,Main_pattern_2])  # first item corresponds to the main pattern
Num_samples_1 = Num_samples_2 = int(Num_samples/2)
MaxPerturbSize = int(num_polarity_levels/2)-5
# MaxPerturbSize = (mid-AttrLevelLow)-1
for i in range(Num_samples_1 - 1):  # Note the main pattern, not included here, is a correct sample
    Inputs.append(PerturbPattern(Main_pattern_1,MaxPerturbSize))
    Outputs.append(Main_pattern_1)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
for i in range(Num_samples_2 - 1):
    Inputs.append(PerturbPattern(Main_pattern_2,MaxPerturbSize))
    Outputs.append(Main_pattern_2)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list

InputCells = list(range(np.prod(Layering[0])))
OutputCells = InputCells

# Network parameters
CreateNetwork = True  # true when there is no interaction with GA, for example, while using backpropagation only
LoadNetwork = False  # because network is created by the GA module
NodeList = []
SequentialInputs = True  # should we have this as an argument??
InputSequenceLength = 10
individual = 0
NetworkParams = []
StartTime = time.time()

# Learning arguments
LearnNodeList = []
LearnEdgeList = []

# Simulation arguments
BlockNodes = []
EvaluationProp = 0.1

arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
                     NumSimIters,SimParamFiles,NodeList,\
                     LearnNodeList,LearnEdgeList,\
                     Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
                     dgj_scale,vm_scale,time_step,EvaluationProp,\
                     SaveFile,SaveFileVersion,JobId,\
                     RecordVisualData,\
                     Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
                     individual,StartTime,WallTime,\
                     BlockNodes]

simBEN = SimBEN(arguments)
simBEN.MasterLearn()

