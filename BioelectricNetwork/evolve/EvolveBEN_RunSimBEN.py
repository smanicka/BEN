from BioelectricNetwork.ben.SimBEN import SimBEN
import torch
import sys

# Parse arguments
args = sys.argv
Individual = int(args[1])-1  # this will be the slurm job array id
SaveFileVersion = args[2]
JobId = args[3]

FileLoaded = False
while not FileLoaded:
	try:
		AllArguments = torch.load('Data/BENArguments' + SaveFileVersion + '.dat')
	except:
		print('BENArguments file not found')
	else:
		FileLoaded = True

arguments = AllArguments[Individual]  # 'arguments' is populated by EvolveBEN_LogicGate_XX.py

if '999' in arguments:
	idx = arguments.index('999')
	arguments[idx] = JobId  # replace '999' with the actual slurm job id

simBEN = SimBEN(arguments)
simBEN.MasterLearn()


