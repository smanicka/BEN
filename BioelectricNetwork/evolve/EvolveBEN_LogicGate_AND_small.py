import sys
import ast
import numpy as np
from BioelectricNetwork.evolve.GA import GA
import torch
import time
import os
import glob

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd 
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# GA parameters
PopSize = int(args[16])
RecombinationRate = ast.literal_eval(args[17])
MutationRate = ast.literal_eval(args[18])
DemeSizeProp = ast.literal_eval(args[19])
MeanWeightMutation = ast.literal_eval(args[20])
MeanBiasMutation = ast.literal_eval(args[21])
# Storage parameters
SimParamFiles = args[22]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[23]
SaveFileVersion = args[24]
JobId = args[25]
RecordVisualData = ast.literal_eval(args[26])
Seed = ast.literal_eval(args[27])
SeedFile = args[28]
SeedNoise = ast.literal_eval(args[29])
WallTime = args[30]
epoch = int(args[31])

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)
Input_1 = [low,low]
Input_2 = [low,high]
Input_3 = [high,low]
Input_4 = [high,high]
Inputs = [Input_1,Input_2,Input_3,Input_4]
Output_1 = [low]
Output_2 = [low]
Output_3 = [low]
Output_4 = [high]
Outputs = [Output_1,Output_2,Output_3,Output_4]

InputCells = list(range(np.prod(Layering[0])))
# OutputCells = InputCells.copy()
NumCells = int(np.sum([np.prod(x) for x in Layering]))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))

# Other GA parameters
LatticeDims = None
ConstructLatticeLayer = False
AllowIntraLyerConnections = False
InterLayerConnectionDensity = 1.0  # full layer-layer connectivity

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA
LoadNetwork = True  # because network is created by the GA module
NodeList = []
SequentialInputs = True
InputSequenceLength = 10
EvaluationProp = 0.1

def ClearFiles():
    Fnames = './Data/LogicGateEvolveANDSmall_*.dat'
    fnames = glob.glob(Fnames)
    for fname in fnames:
        if os.path.isfile(fname):
            os.remove(fname)
    fname = './Data/GAResults' + SaveFileVersion + '.dat'
    if os.path.isfile(fname):
            os.remove(fname)
    fname = './Data/BENArguments' + SaveFileVersion + '.dat'
    if os.path.isfile(fname):
            os.remove(fname)

def ClearBENFiles():
    for individual in range(PopSize):
        fname = './Data/LogicGateEvolveANDSmall_' + str(individual) + '_' + SaveFileVersion +'.dat'
        if os.path.isfile(fname):
            os.remove(fname)

def SaveResults(Population,Scores):
    WeightGenotype, BiasGenotype = Population
    NewResults = [WeightGenotype, BiasGenotype, Scores]
    ResultsFile = './Data/GAResults' + SaveFileVersion + '.dat'
    if os.path.isfile(ResultsFile):
        Results = torch.load(ResultsFile)
    else:
        Results = []
    Results.append(NewResults)
    torch.save(Results,ResultsFile)

def RetrieveBackproppedPopulation(ga):
    Scores = torch.FloatTensor([-99]*PopSize)
    WeightGenotype = torch.FloatTensor(np.random.uniform(-1,1,(num_cells,num_cells)))
    WeightGenotype = WeightGenotype.repeat((1,PopSize))
    BiasGenotype = torch.FloatTensor(np.random.uniform(0,1,num_cells))
    BiasGenotype = BiasGenotype.repeat((PopSize))
    for individual in range(PopSize):
        fname = './Data/LogicGateEvolveANDSmall_' + str(individual) + '_' + SaveFileVersion + '.dat'
        try:
            _,_,_,_,_,_,BestWeights,BestBias,BestWeightMatrix,_,BestErrors,_ = torch.load(fname)
        except:
            print('File ', fname, ' does not exist.')
            continue  # NOTE: without 'continue', the loop will break by default
        else:
            # Update population with the best parameters obtained from backpropagation
            st1 = 0
            nd1 = num_cells
            st2 = individual*num_cells
            nd2 = st2 + num_cells
            BestW = BestWeightMatrix.clone()
            BestB = BestBias.clone()
            WeightGenotype[st1:nd1,st2:nd2] = BestW
            BiasGenotype[st2:nd2] = BestB
            Scores[individual] = BestErrors[-1]
    ga.Population = [WeightGenotype,BiasGenotype]
    return(Scores)

def GenerateBENArguments(Population):
    WeightGenotype, BiasGenotype = Population
    BENArguments = []
    for individual in range(PopSize):
        st1 = 0
        nd1 = num_cells
        st2 = individual*num_cells
        nd2 = st2 + num_cells
        W = WeightGenotype[st1:nd1,st2:nd2].clone()
        B = BiasGenotype[st2:nd2].clone() 
        NetworkParams = [W,B]
        LearnNodeList = []
        LearnEdgeList = []
        BlockNodes= []
        arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
                     NumSimIters,SimParamFiles,NodeList,\
                     LearnNodeList,LearnEdgeList,\
                     Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
                     dgj_scale,vm_scale,time_step,EvaluationProp,\
                     SaveFile,SaveFileVersion,JobId,\
                     RecordVisualData,\
                     Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
                     individual,StartTime,WallTime,\
                     BlockNodes]
        BENArguments.append(arguments)
    BENFile = './Data/BENArguments' + SaveFileVersion + '.dat'
    torch.save(BENArguments,BENFile)

if __name__ == '__main__':
    StartTime = time.time()
    ga_args = [PopSize,Layering,LatticeDims,RecombinationRate,MutationRate,DemeSizeProp,MeanWeightMutation,MeanBiasMutation,
               ConstructLatticeLayer,AllowIntraLyerConnections,InterLayerConnectionDensity,Seed,SeedFile,SeedNoise]
    ga = GA(ga_args)
    print('epoch = ',epoch)
    if epoch == 0:   # Generate population
        ClearFiles()
        ga.GeneratePopulation() 
    else:   # create new generation with new network structures and parameters: new layering, new AdjMatrix for the reservoir, new NetworkParams etc.
        Scores = RetrieveBackproppedPopulation(ga)  # load results file from the SimBEN processes
        SaveResults(ga.Population,Scores)
        ga.MicrobialTournament(Scores)
    GenerateBENArguments(ga.Population)  
    ClearBENFiles()


