import sys
import ast
import torch
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import matplotlib.pyplot as plt
import time
import matplotlib
import matplotlib.cm as cm
from matplotlib.collections import LineCollection

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]
GateId = int(args[22])
Net = args[23]

AllGates = ['NOT','AND','OR','XOR','Multi','PatternReg']
Gate = AllGates[GateId]

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)

if ((Gate=='AND') | (Gate=='OR') | (Gate=='XOR')):
	Input_1 = [low,low]
	Input_2 = [low,high]
	Input_3 = [high,low]
	Input_4 = [high,high]
	Input_5 = [mid,mid]
	# Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5]
	Inputs = [Input_1,Input_2,Input_3,Input_4]
	# Inputs = [Input_4,Input_1,Input_2,Input_3]
	if Gate=='AND':
		Output_1 = [low]
		Output_2 = [low]
		Output_3 = [low]
		Output_4 = [high]
		Output_5 = [mid]
	elif Gate=='OR':
		Output_1 = [low]
		Output_2 = [high]
		Output_3 = [high]
		Output_4 = [high]
	elif Gate=='XOR':
		Output_1 = [low]
		Output_2 = [high]
		Output_3 = [high]
		Output_4 = [low]
	Outputs = [Output_1,Output_2,Output_3,Output_4]
	# Outputs = [Output_4,Output_1,Output_2,Output_3]
	# Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5]
elif Gate=='NOT':
	Input_1 = [low]
	Input_2 = [high]
	Input_3 = [mid]
	# Input_2 = [mid+10]   # TESTING ONLY
	# Inputs = [Input_1,Input_2,Input_3]
	Inputs = [Input_1,Input_2]
	Output_1 = [high]
	Output_2 = [low]
	Output_3 = [mid]
	# Outputs = [Output_1,Output_2,Output_3]
	Outputs = [Output_1,Output_2]
elif Gate=='Multi':
	# OR
	Input_1 = [low,low,low]
	Input_2 = [low,high,low]
	Input_3 = [high,low,low]
	Input_4 = [high,high,low]
	# AND
	Input_5 = [low,low,high]
	Input_6 = [low,high,high]
	Input_7 = [high,low,high]
	Input_8 = [high,high,high]
	Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5,Input_6,Input_7,Input_8]
	# OR
	Output_1 = [low]
	Output_2 = [low]
	Output_3 = [low]
	Output_4 = [high]
	# AND
	Output_5 = [low]
	Output_6 = [high]
	Output_7 = [high]
	Output_8 = [high]
	Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5,Output_6,Output_7,Output_8]
elif Gate=='PatternReg':
	## Method 1  #3-cell network
	# Num_samples = 2
	# def PerturbPattern(pattern,delta,group):
	# 	num_pixels = len(pattern)
	# 	if (group == 1):
	# 		delta_index_range = np.arange(-delta,1)
	# 	elif (group == 2):
	# 		delta_index_range = np.arange(0,delta+1)
	# 	perturbation = np.random.choice(delta_index_range,num_pixels)
	# 	new_indices = (pattern + perturbation)
	# 	return(list(new_indices))  # turn np array into a regular list
	# Main_pattern_1 = [high,high,high]
	# Main_pattern_2 = [low,low,low]
	# Inputs = list([Main_pattern_1,Main_pattern_2])  # Note that "Inputs" contains indices into "Polarity_levels", not the polarities themselves
	# Outputs = list([Main_pattern_1,Main_pattern_2])  # first item corresponds to the main pattern
	# Num_samples_1 = Num_samples_2 = int(Num_samples/2)
	# MaxPerturbSize = int(num_polarity_levels/2) - 2
	# for i in range(Num_samples_1 - 1):  # Note the main pattern, not included here, is a correct sample
	# 	Inputs.append(PerturbPattern(Main_pattern_1,MaxPerturbSize,1))
	# 	Outputs.append(Main_pattern_1)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
	# for i in range(Num_samples_2 - 1):
	# 	Inputs.append(PerturbPattern(Main_pattern_2,MaxPerturbSize,2))
	# 	Outputs.append(Main_pattern_2)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list

	## Method 2  #3-cell network
	# Num_samples = 2
	# Main_pattern_1 = [high-1,high-2,high-3]
	# Inputs = list([Main_pattern_1])  # Note that "Inputs" contains indices into "Polarity_levels", not the polarities themselves
	# Outputs = list([Main_pattern_1])  # first item corresponds to the main pattern
	# for i in range(Num_samples - 1):  # Note the main pattern, not included here, is a correct sample
	# 	PerturbedPattern = [low+1,low+2,low+3]
	# 	Inputs.append(PerturbedPattern)
	# 	Outputs.append(Main_pattern_1)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list

	## Method 3  #2-cell network
	Inputs = [[high,low],[high,high],[low,high],[mid,mid]]
	Outputs = Inputs  # NOTE: We actually don't know what the correct outputs should be here, but it doesn't matter anyway

InputCells = list(range(np.prod(Layering[0])))
# OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))
OutputCells = InputCells.copy()

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA, or when
LoadNetwork = True  # should be True for both simulation and learning
NodeList = []
SequentialInputs = True
InputSequenceLength = 1
EvaluationProp = 0.1
individual = 0
StartTime = time.time()

if LoadNetwork:
	data = torch.load('./Data/'+SimParamFiles[0])
	if (len(data)==12):
		_, _, _, InitWeights, InitBias, InitW, BestWeights, BestBias, BestW, _, _, _ = data  # Newest format has length 12
	else:
		print('Wrong file format')
	NetworkParams = [BestW.clone(),BestBias.clone()]
else:
	NetworkParams = []

TurnOffGapJuncGating = False
TurnOffSignalNonlinearity = False
BlockNodes = []
LearnNodeList = []
LearnEdgeList = []
BlockNodes = []

arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
			 NumSimIters,SimParamFiles,NodeList,\
			 LearnNodeList,LearnEdgeList,\
			 Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
			 dgj_scale,vm_scale,time_step,EvaluationProp,\
			 SaveFile,SaveFileVersion,JobId,\
			 RecordVisualData,\
			 Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
			 individual,StartTime,WallTime,\
			 BlockNodes]

simBEN = SimBEN(arguments)

simBEN.MasterLearn()

print(simBEN.ben.MeanError)

vm_time = simBEN.ben.vm_time_all
cc_sig_time = simBEN.ben.cc_sig_time_all
W_time = simBEN.ben.Dgj_time_all
NumEdges = simBEN.ben.NumEdges
EdgeList = np.array(torch.nonzero(torch.triu(simBEN.ben.BaseAdj)))
n = simBEN.ben.n
NumInputStates = vm_time.shape[1]

def PrepareDataBlock(state,weight):
	NumSteps, NumSamples, NumCells = state.shape
	NumStepRepeats = 1
	StateTimeSeries = torch.FloatTensor([])
	WeightTimeSeries = torch.FloatTensor([])
	for i in range(InputSequenceLength):
		offset = int(DelayProp*NumSimIters[0])
		RestStart = (i*NumSimIters[0])
		PatternStart = RestStart + offset
		RestPattern = state[RestStart:PatternStart,:,:]
		InitPattern = state[PatternStart,:,:]
		InitPattern = InitPattern.repeat(NumStepRepeats,1).view(NumStepRepeats,NumSamples,NumCells)
		st = PatternStart+1
		nd = RestStart+NumSimIters[0]
		EvolPattern = state[st:nd,:,:]
		StateTimeSeries = torch.cat((StateTimeSeries,RestPattern,InitPattern,EvolPattern),dim=0)
		RestWeight = weight[RestStart:PatternStart,:,:]
		InitWeight = weight[PatternStart,:,:]
		InitWeight = InitWeight.repeat(NumStepRepeats,1).view(NumStepRepeats,NumSamples,NumEdges)
		EvolWeight = weight[st:nd,:,:]
		WeightTimeSeries = torch.cat((WeightTimeSeries,RestWeight,InitWeight,EvolWeight),dim=0)
	data = torch.cat((StateTimeSeries,WeightTimeSeries),dim=2)
	return(data)

Data = PrepareDataBlock(vm_time,W_time)
NumSteps, NumSamples, _ = Data.shape
for sample_id in range(4):
	sequence_num = 0
	# time_step = (sequence_num*NumSimIters[0]) + NumSimIters[0] - 100
	if sample_id < 3:  # visualize the final states
		time_step = (sequence_num*NumSimIters[0]) + NumSimIters[0] - 1
	else:  # visualize the initial state
		time_step = 1
	data = np.asarray(Data[time_step,sample_id,:].data)

	y_sides = [sides[0] for sides in Layering]
	x_sides = [sides[1] for sides in Layering]

	# YOffset = [3.5, 0.4, 1.9]  # this needs some manual tuning depending on the number of layers
	# YOffset = [0, 1.4]  # this needs some manual tuning depending on the number of layers
	YOffset = [3]

	CreateYCoords = lambda x_side,y_side,offset: np.ravel([np.linspace(i+offset,y_side+i+offset,y_side) for i in range(x_side)])
	y = list(map(CreateYCoords,x_sides,y_sides,YOffset))
	y = np.hstack(y)

	LayerIndices = list(range(num_layers))
	CoordStart = [sum(x_sides[0:i]) for i in LayerIndices]
	CreateXCoords = lambda st,x_side,y_side: np.ravel([np.repeat(st+i,y_side) for i in range(x_side)])
	x = list(map(CreateXCoords,CoordStart,x_sides,y_sides))
	x = np.hstack(x)

	xypts = np.column_stack((x,y))

	EdgeListNP = np.array(EdgeList)

	edges = xypts[EdgeListNP]
	NumEdges = len(EdgeList)

	fig, ax = plt.subplots(1)
	# Plot input nodes and their outgoing edges
	states = data[0:n]
	states = (states + 0.08)/0.16   # Uncomment this for vm; comment this for cc_sig
	cmm = plt.cm.get_cmap('coolwarm')
	ln = plt.scatter(xypts[:,0],xypts[:,1], s=500, c=states, cmap=cmm, norm=matplotlib.colors.Normalize(0,1))
	lines = LineCollection(edges,color='grey',zorder=0)
	weights = np.abs(data[n:(n+NumEdges)])
	lw = np.array(weights*1/np.max(weights))
	lines.set_linewidth(lw)  # NOTE: set blit=False for this update to take effect

	ax.add_collection(lines)

	ax.set_xticks([])
	ax.set_yticks([])
	ax.set_frame_on(False)
	ax.set_xlim(-0.1,0.1)
	ax.set_ylim(2,7)  # 4.5, 6.5
	# ax.set_clip_box(Bbox([[-0.5,5.5],[-0.5,6.5]]))
	if sample_id == 2:
		clb = plt.colorbar(ln,ticks=[0.0,0.5,1.0],shrink=0.8,anchor=(0.0,0.01))
		clb.ax.set_yticklabels(['-80 mV', '-40 mV', '0 mV'])
	fig.tight_layout()
	plt.show()
	fname = 'Figures/NetworkState' + str(sample_id) + '.png'
	# fname = 'Figures/Final2.png'
	fig.savefig(fname,bbox_inches='tight')































