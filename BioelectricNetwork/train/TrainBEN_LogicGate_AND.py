import sys
import ast
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import time

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd 
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)
Input_1 = [low,low]
Input_2 = [low,high]
Input_3 = [high,low]
Input_4 = [high,high]
# Input_5 = [mid,mid]
# Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5]
Inputs = [Input_1,Input_2,Input_3,Input_4]
Output_1 = [low]
Output_2 = [low]
Output_3 = [low]
Output_4 = [high]
# Output_5 = [mid]
# Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5]
Outputs = [Output_1,Output_2,Output_3,Output_4]

InputCells = list(range(np.prod(Layering[0])))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))

# Network arguments
CreateNetwork = True  # true when there is no interaction with GA, for example, while using backpropagation only
LoadNetwork = False  # because network is created by the GA module
NodeList = []
SequentialInputs = True  # should we have this as an argument??
InputSequenceLength = 10
individual = 0
NetworkParams = []
StartTime = time.time()

# Learning arguments
LearnNodeList = []
LearnEdgeList = []

# Simulation arguments
BlockNodes = []

arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
             NumSimIters,SimParamFiles,NodeList,\
             LearnNodeList,LearnEdgeList,\
             Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
             dgj_scale,vm_scale,time_step,\
             SaveFile,SaveFileVersion,JobId,\
             RecordVisualData,\
             Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
             individual,StartTime,WallTime,\
             BlockNodes]

simBEN = SimBEN(arguments)
simBEN.MasterLearn()

