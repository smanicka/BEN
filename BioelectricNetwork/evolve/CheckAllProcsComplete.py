import torch
import numpy as np
import sys
import re
import os

args = sys.argv
PopSize = int(args[1]) 
NumBackpropIters = int(args[2])
SaveFileVersion = args[3]
SaveFilePrefix = args[4]
CutoffTime = args[5]
ElapsedTime = int(args[6])

CutoffTime = np.array(list(map(int,re.split(':|-',CutoffTime))))
TimeCoeffs = np.array([24*60*60,60*60,60,1])
CutoffTimeSecs = np.sum(CutoffTime*TimeCoeffs)

# If some but not all backprop jobs have completed, due to which the script is waiting, then make corrections to the results files (copy contents from other files) so the script can continue.
# This assumes that there's at least one correct data file.
if (ElapsedTime > CutoffTimeSecs):
	AllFilesFixed = False
	while not AllFilesFixed:
		numgoodfiles = 0
		for individual in range(PopSize):
			fname = './Data/' + SaveFilePrefix + '_' + str(individual) + '_' + SaveFileVersion +'.dat'
			try:
				contents = torch.load(fname)
			except:  # if file does not exist, create file using contents from previous file
				try:
					prevcontents
				except:
					continue
				else:
					torch.save(prevcontents,fname)
					numgoodfiles += 1
				continue
			else:
				numgoodfiles += 1
				itn = contents[0]
				if (itn != (NumBackpropIters-1)):  # simply set the itn to the desired value, so the script can continue without explicit intervention
					itn = (NumBackpropIters-1)
					contents[0] = itn
					torch.save(contents,fname)
				prevcontents = contents.copy()
		if numgoodfiles == PopSize:
			AllFilesFixed = True

CheckValues = np.array([False]*PopSize)
AllDone = False
while not AllDone:
	for individual in range(PopSize):
		fname = './Data/' + SaveFilePrefix + '_' + str(individual) + '_' + SaveFileVersion + '.dat.DONE'
		if os.path.isfile(fname):
			CheckValues[individual] = True
			os.remove(fname)
		else:
			continue
	if np.all(CheckValues):
		AllDone = True
	else:
		AllDone = False
