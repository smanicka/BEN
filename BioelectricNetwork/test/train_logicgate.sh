#@IgnoreInspection BashAddShebang
walltime=0-10:00:00
#python -m BioelectricNetwork.train.TrainBEN_LogicGate_AND 20 [300] LearnFull False True [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateAND1.dat LogicGateAND.dat 999 999 False $walltime
#python -m BioelectricNetwork.train.TrainBEN_LogicGate_NOT 20 [300] LearnFull False True [[1,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateNOT1.dat LogicGateNOT.dat 999 999 False $walltime
#python -m BioelectricNetwork.train.TrainBEN_LogicGate_OR 20 [300] LearnFull False True [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateOR1.dat LogicGateOR.dat 999 999 False $walltime
#python -m BioelectricNetwork.train.TrainBEN_LogicGate_XOR 20 [300] LearnFull False True [[2,1],[2,1],[1,1]] True False [] False 0.0 1.0 1.0 1.0 1.0 LogicGateXOR1.dat LogicGateXOR.dat 999 999 False $walltime
python -m BioelectricNetwork.train.TrainBEN_LogicGate_NAND 20 [300] LearnPartial False True [[10,1]] True False [] False 0.2 1.0 1.0 1.0 0.5 [LogicGateAND_0_822.dat,LogicGateNOT_0_737.dat] LogicGateNAND.dat 999 999 False $walltime