# Computes lattice adjacency matrices, shuffles them according to some rewiring probability, and passes them to the BEN module.
# Presently, we assume that the network has only three layers - 1 input, 1 "reservoir" and 1 output layer.

import torch
import numpy as np
import itertools as itr

class GA():

    def __init__(self,arguments): 
        self.PopSize = arguments[0]
        self.Layering = arguments[1]
        self.LatticeDims = arguments[2]
        self.RecombinationRate = arguments[3]
        self.MutationRate = arguments[4]
        self.DemeSizeProp = arguments[5]
        self.MeanWeightMutation = arguments[6]
        self.MeanBiasMutation = arguments[7]
        self.ConstructLatticeLayer = arguments[8]
        self.AllowIntraLyerConnections = arguments[9]
        self.InterLayerConnectionDensity = arguments[10]  # proportion of reservoir nodes to connect from other layers; may have to be turned into an argument in the future
        self.Seed = arguments[11]
        self.SeedFile = arguments[12]
        self.SeedNoise = arguments[13]
        self.latticeRewireProbs = np.linspace(0,1,self.PopSize)  # helps create a set of reservoir structures ranging from lattice-like to random
        self.cells_per_layer = [np.prod(x) for x in self.Layering]
        self.num_layers = len(self.cells_per_layer)
        self.max_num_cells = int(np.sum(self.cells_per_layer)) # total number of cells in network (all networks have the same number of max cells)
        self.AllCellIndices = list(range(self.max_num_cells))
        self.CellIndicesPerLayer = []
        Curr = 0
        for i in self.cells_per_layer:
            self.CellIndicesPerLayer.append(self.AllCellIndices[Curr:(Curr+i)])
            Curr += i
        self.DemeSize = int(np.ceil(self.DemeSizeProp*self.PopSize))  # population is distributed on a ring-geography; 'deme' size is 5% of the ring size
        self.computeAdjMatricesIndexPairs()
        self.ComputeAdjacencyMatrixSkeleton()
        self.CrossoverSegSize = int(np.ceil(self.RecombinationRate*self.numNodePairs))  # 10% of the number of entries in the upper triangle of the genotype matrix
        self.Winner = 99
        self.WinnerWeight = torch.FloatTensor([99]*self.max_num_cells*self.max_num_cells).view(self.max_num_cells,self.max_num_cells)

    # Compute an adjacency matrix that contains all possible inter-layer connections and no intra-layer connections
    def ComputeAdjacencyMatrixSkeleton(self):
        self.AdjacencyMatrixSkeleton = np.zeros((self.max_num_cells,self.max_num_cells),dtype=np.uint8)
        LayerPairs = [(0,1),(2,1)]  # for now we assume three layers only
        for layer1,layer2 in LayerPairs:
            start1,end1 = self.CellIndicesPerLayer[layer1][0], self.CellIndicesPerLayer[layer1][-1]+1
            start2,end2 = self.CellIndicesPerLayer[layer2][0], self.CellIndicesPerLayer[layer2][-1]+1
            self.AdjacencyMatrixSkeleton[start1:end1,start2:end2] = 1
            self.AdjacencyMatrixSkeleton[start2:end2,start1:end1] = 1
        self.AdjacencyMatrixSkeleton = torch.FloatTensor(self.AdjacencyMatrixSkeleton)

    def computeAdjMatricesIndexPairs(self):
        nodes = list(range(self.max_num_cells))
        nodePairs = itr.combinations(nodes,2)  # unique node-pair combinations
        self.nodePairs = np.array(list(nodePairs))
        self.numNodePairs = self.nodePairs.shape[0]

    # We assume that layer2 is the layer that is connected "to" and hence needs checking that cells in that layer are connected already 
    # (we don't want to connect to isolated cells)
    def computeRandomInterLayerAdjacenyMatrix(self,layer1,layer2,FullAdjMatrix):
        numLayer1Cells = self.cells_per_layer[layer1]
        numLayer2Cells = self.cells_per_layer[layer2]
        numAdjEntries = numLayer1Cells*numLayer2Cells
        numOnes = np.int(np.ceil(self.InterLayerConnectionDensity*numAdjEntries))
        numOnesPerCell = np.int(np.floor(numOnes/numLayer1Cells))
        start,end = self.CellIndicesPerLayer[layer2][0], self.CellIndicesPerLayer[layer2][-1]+1
        if self.AllowIntraLyerConnections:
            ConnectedCells = np.array(self.CellIndicesPerLayer[layer2])[np.where(np.ndarray.any(FullAdjMatrix[start:end,start:end],axis=0))[0]]
        else:
            ConnectedCells = np.array(self.CellIndicesPerLayer[layer2])  # connect to all cells in layer2 since it's not connected within itself
        ConnectedCells -= start
        AdjMatrix = np.zeros((numLayer1Cells,numLayer2Cells))
        offset = self.CellIndicesPerLayer[layer1][0]
        for cell in self.CellIndicesPerLayer[layer1]:
            ToCells = np.random.choice(ConnectedCells,numOnesPerCell,replace=False)
            AdjMatrix[cell-offset,ToCells] = 1
        return(AdjMatrix)

    def computeLatticeAdjacencyMatrix(self):  # lattice connectivity for the reservoir layer
        numrows = self.LatticeDims[0]
        numcols = self.LatticeDims[1]
        rowIndices = np.tile(np.arange(0,numrows),numcols)
        colIndices = np.repeat(np.arange(0,numcols),numrows)
        numCellsLayer = numrows * numcols
        cellIndices = np.arange(numCellsLayer)
        AdjMatrix = np.zeros((numCellsLayer,numCellsLayer),dtype=np.int8)
        for cell in cellIndices:
            r = rowIndices[cell]
            c = colIndices[cell]
            rowNeighbors = cellIndices[(rowIndices==r) & (np.abs(colIndices-c)==1)]
            colNeighbors = cellIndices[(colIndices==c) & (np.abs(rowIndices-r)==1)]
            AdjMatrix[cell,rowNeighbors] = 1
            AdjMatrix[cell,colNeighbors] = 1
        return(AdjMatrix)

    def shuffleLatticeAdjacencyMatrix(self,LatticeAdj,RewireProb):  # shuffle connectivity for the reservoir layer
        numrows = self.LatticeDims[0]
        numcols = self.LatticeDims[1]
        numCellsLayer = numrows * numcols
        cellIndices = np.arange(numCellsLayer)
        for cell in cellIndices:
            row = LatticeAdj[cell,]
            onesidx = np.where(row==1)[0]
            zerosidx = np.where(row==0)[0]
            if (len(zerosidx)>1):  # it's possible that repeated shuffling leaves some nodes either fully connected or isolated
                zerosord = np.arange(0,len(zerosidx))
                shuffled = False
                for i in range(len(onesidx)):
                    if shuffled:
                        shuffled = False
                        continue
                    pos = onesidx[i]
                    p = np.random.uniform(0,1)
                    if (p <= RewireProb):
                        newpos = cell
                        while (newpos==cell):
                            j = np.random.choice(zerosord)
                            newpos = zerosidx[j]
                        LatticeAdj[cell,pos] = 0
                        LatticeAdj[pos,cell] = 0
                        LatticeAdj[cell,newpos] = 1
                        LatticeAdj[newpos,cell] = 1
                        onesidx[i] = newpos
                        zerosidx[j] = pos
                        shuffled = True
        return(LatticeAdj)

    def computeFullAdjacencyMatrix(self,IndividualId):
        # This is the base Adjacency matrix; the actual Adj might change during the experiment
        FullAdjMatrix = np.zeros((self.max_num_cells,self.max_num_cells),dtype=np.uint8)
        if self.ConstructLatticeLayer:
            # Compute lattice connectivity for the "reservoir" layer, and shuffle connectivity according to rewiring probability
            LatticeAdjMatrix = self.computeLatticeAdjacencyMatrix()  # currently we assume that only the reservoir layer is a lattice
            LatticeAdjMatrix = self.shuffleLatticeAdjacencyMatrix(LatticeAdjMatrix.copy(),self.latticeRewireProbs[IndividualId])
            LatticeLayer = 1
            start, end = self.CellIndicesPerLayer[LatticeLayer][0], self.CellIndicesPerLayer[LatticeLayer][-1]+1
            FullAdjMatrix[start:end,start:end] = LatticeAdjMatrix
        # Compute random connectivity between adjacent pairs of layers
        LayerPairs = [(0,1),(2,1)]   # for now we assume three layers only
        for layer1,layer2 in LayerPairs:
            InterLayerAdjMatrix = self.computeRandomInterLayerAdjacenyMatrix(layer1,layer2,FullAdjMatrix)
            start1,end1 = self.CellIndicesPerLayer[layer1][0], self.CellIndicesPerLayer[layer1][-1]+1
            start2,end2 = self.CellIndicesPerLayer[layer2][0], self.CellIndicesPerLayer[layer2][-1]+1
            FullAdjMatrix[start1:end1,start2:end2] = InterLayerAdjMatrix
            FullAdjMatrix[start2:end2,start1:end1] = np.transpose(InterLayerAdjMatrix)            
        return(FullAdjMatrix)

    def GeneratePopulation(self):
        if self.Seed:
            _, _, _, _, _, _, _, BestBias, BestW, _, _, _ = torch.load('./Data/'+self.SeedFile)  # this is the latest format
            WeightGenotype = np.tile(BestW,self.PopSize)
            WeightGenotype += np.random.normal(0,self.SeedNoise,(self.max_num_cells,self.PopSize*self.max_num_cells))
            WeightGenotype = torch.FloatTensor(WeightGenotype)
            BiasGenotype = np.tile(BestBias,self.PopSize)
            BiasGenotype += np.random.normal(0,self.SeedNoise,self.PopSize*self.max_num_cells)
            BiasGenotype = torch.FloatTensor(BiasGenotype)
            BiasGenotype[BiasGenotype > 1] = 1
            BiasGenotype[BiasGenotype < 0] = 0
        else:
            WeightGenotype = torch.FloatTensor(np.random.uniform(-1,1,(self.max_num_cells,self.PopSize*self.max_num_cells)))  # random parameters for each individual
            BiasGenotype = torch.FloatTensor(np.random.uniform(0,1,self.PopSize*self.max_num_cells))
        IndividualIds = list(range(self.PopSize))
        # shuffle the ordering of the "demes" so that it's not always the case that similarly rewired nets compete;
        # note however that this ordering will be fixed throughout the epochs.
        self.IndividualMappedIds = IndividualIds.copy()  
        np.random.shuffle(self.IndividualMappedIds)
        for individual in range(self.PopSize):
            FullAdjMatrix = self.computeFullAdjacencyMatrix(self.IndividualMappedIds[individual])
            st1 = 0
            nd1 = self.max_num_cells
            st2 = individual*self.max_num_cells
            nd2 = st2 + self.max_num_cells
            FullAdjMatrix = torch.FloatTensor(FullAdjMatrix)
            W = WeightGenotype[st1:nd1,st2:nd2]
            W = W * FullAdjMatrix.clone()
            WeightGenotype[st1:nd1,st2:nd2] = (W+W.t())/2.0  # ensures that W is symmetric
        self.Population = [WeightGenotype,BiasGenotype]

    def SelectPairs(self):
        NumIndividuals = self.PopSize
        Individuals = np.array(list(range(NumIndividuals)))
        splitIdx = np.random.randint(0,NumIndividuals)
        # slice and splice the individuals randomly, so that the pairing is not always biased against the later individuals
        Individuals = np.concatenate((Individuals[splitIdx:],(Individuals[0:splitIdx])))
        localDemeSize = self.DemeSize
        IndividualPairs = []
        while len(Individuals) > 0:
            A = Individuals[0]
            localDemeSize = min(localDemeSize,NumIndividuals)
            if localDemeSize > 1:
                MateIndices = np.remainder(list(range(1,localDemeSize)),NumIndividuals)  # random choice from a contiguous piece of the circle next to A
            else:
                MateIndices = np.array([1])  # to make this work, ensure that PopSize is an even number
            MateInd = int(np.random.choice(MateIndices,1))
            B = Individuals[MateInd]
            Individuals = np.delete(Individuals,0)
            Individuals = np.delete(Individuals,MateInd-1)
            NumIndividuals -= 2
            IndividualPairs.append([A,B])
        return(IndividualPairs)

    def Crossover(self,winner,loser,WeightGenotype,BiasGenotype):  # horizontal gene transfer
        st1 = 0
        nd1 = self.max_num_cells
        st2 = winner*self.max_num_cells
        nd2 = st2 + self.max_num_cells
        WinnerWeight = WeightGenotype[st1:nd1,st2:nd2]
        self.Winner = winner
        self.WinnerWeight = WinnerWeight.clone()
        WinnerBias = BiasGenotype[st2:nd2]
        st3 = loser*self.max_num_cells
        nd3 = st3 + self.max_num_cells
        LoserWeight = WeightGenotype[st1:nd1,st3:nd3]
        LoserBias = BiasGenotype[st3:nd3]
        # Pick a random segment from the upper triangle of the Weight genotype matrix
        startIndex = np.random.randint(0,self.numNodePairs-self.CrossoverSegSize+1)
        weightGenotypeSegmentIndices = self.nodePairs[startIndex:(startIndex+self.CrossoverSegSize),]  # nodePairs doesn't include self-loop pairs
        # Cross-over (horizontal transfer of segment from winner to loser)
        x,y = weightGenotypeSegmentIndices[:,0], weightGenotypeSegmentIndices[:,1]
        LoserWeight[x,y] = WinnerWeight[x,y].clone()
        LoserWeight[y,x] = WinnerWeight[y,x].clone()  # ensures that weight matrix is symmetric
        biasGenotypeSegmentIndices = np.unique(weightGenotypeSegmentIndices)  # for bias transfer, pick all cells involved in weight transfer
        LoserBias[biasGenotypeSegmentIndices] = WinnerBias[biasGenotypeSegmentIndices].clone()
        # reinsert into population
        WeightGenotype[st1:nd1,st3:nd3] = LoserWeight
        BiasGenotype[st3:nd3] = LoserBias

    def Mutate(self,loser,WeightGenotype,BiasGenotype):
        st1 = 0
        nd1 = self.max_num_cells
        st3 = loser*self.max_num_cells
        nd3 = st3 + self.max_num_cells
        LoserWeight = WeightGenotype[st1:nd1,st3:nd3]
        LoserBias = BiasGenotype[st3:nd3]
        weightMutationInds = torch.ByteTensor(np.random.binomial(1,self.MutationRate,(self.numNodePairs)))  # randomly choose mutation candidates
        if weightMutationInds.any():  # mutate only if there are candidates chosen for mutation
            weightGenotypeSegmentIndices = self.nodePairs[weightMutationInds]
            x,y = weightGenotypeSegmentIndices[:,0], weightGenotypeSegmentIndices[:,1]
            ToMutateWeights = LoserWeight[x,y]
            MutatedWeights = torch.FloatTensor(np.random.normal(ToMutateWeights,self.MeanWeightMutation))
            LoserWeight[x,y] = MutatedWeights
            LoserWeight[y,x] = MutatedWeights  # ensures that weight matrix is symmetric
            biasGenotypeSegmentIndices = np.unique(weightGenotypeSegmentIndices)  # for bias transfer, pick all cells involved in weight transfer
            ToMutateBiases = LoserBias[biasGenotypeSegmentIndices]
            MutatedBiases = torch.FloatTensor(np.random.normal(ToMutateBiases,self.MeanBiasMutation))
            MutatedBiases[MutatedBiases > 1] = 1
            MutatedBiases[MutatedBiases < 0] = 0
            LoserBias[biasGenotypeSegmentIndices] = MutatedBiases
            # reinsert into population
            WeightGenotype[st1:nd1,st3:nd3] = LoserWeight
            BiasGenotype[st3:nd3] = LoserBias
            if not self.AllowIntraLyerConnections:  # impose constraints, for e.g., don't allow intra-layer connections
                WeightGenotype[st1:nd1,st3:nd3] = WeightGenotype[st1:nd1,st3:nd3]*self.AdjacencyMatrixSkeleton

    def MicrobialTournament(self,Scores):
        WeightGenotype, BiasGenotype = self.Population
        IndividualPairs = self.SelectPairs()
        for A,B in IndividualPairs:
            ScoreA = Scores[A]
            ScoreB = Scores[B]
            if ScoreA < ScoreB:   # Score = BEN sim error; thus lower errors means better scores
                winner = A
                loser = B
            elif ScoreB < ScoreA:
                winner = B
                loser = A
            else:
                continue
            self.Crossover(winner,loser,WeightGenotype,BiasGenotype)
            self.Mutate(loser,WeightGenotype,BiasGenotype)
