import sys
import os

args = sys.argv
PopSize = int(args[1]) 
SaveFileVersion = args[2]
SaveFilePrefix = args[3]

for individual in range(PopSize):
	fname = './Data/' + SaveFilePrefix + '_' + str(individual) + '_' + SaveFileVersion +'.dat'
	while os.path.isfile(fname):
		continue

