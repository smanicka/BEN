import sys
import ast
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import time
import torch

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
BridgeLayering = ast.literal_eval(args[6])  # Layering of the bridge only; module layerings are read from the parameter files
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]

# Compute Layering of the full network but concatenating the layering of the modules and the bridge
ModuleLayerings = []
ModuleParamFiles = ['./Data/'+f for f in SimParamFiles]
for i in range(len(ModuleParamFiles)):
    fname = ModuleParamFiles[i]
    contents = torch.load(fname)
    try:
        _, _, ModuleLayering, _, _, _, BestWeights, BestBias, _, _, _, _ = torch.load(fname)
    except:
        print("File does not exist or is not of the correct format")
    ModuleLayerings.append(ModuleLayering)
Layering = []
Layering.extend(ModuleLayerings[0])
Layering.extend(BridgeLayering)  # assuming that there are only two modules
Layering.extend(ModuleLayerings[1])

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)
# Inputs for the whole network
Input_1 = [low,low]
Input_2 = [low,high]
Input_3 = [high,low]
Input_4 = [high,high]
Inputs = [Input_1,Input_2,Input_3,Input_4]
# Output_1 = [high]
# Output_2 = [high]
# Output_3 = [high]
# Output_4 = [low]
# Outputs for the two sockets, one in the upper module is (AND) and the other in the lower module (NOT)
Output_1 = [low,low]
Output_2 = [low,low]
Output_3 = [low,low]
Output_4 = [high,high]
Outputs = [Output_1,Output_2,Output_3,Output_4]
# Specify which layer of the network is to be treated as the "bridge" layer
BridgeLayerIndices = [3]

def computeLatticeEdgeList(layer):  # lattice connectivity within a layer
    numrows = Layering[layer][0]
    numcols = Layering[layer][1]
    rowIndices = np.tile(np.arange(0,numrows),numcols)
    colIndices = np.repeat(np.arange(0,numcols),numrows)
    numCellsLayer = cells_per_layer[layer]
    cellIndices = np.arange(numCellsLayer)
    AdjMatrix = np.zeros((numCellsLayer,numCellsLayer),dtype=np.uint8)
    for cell in cellIndices:
        r = rowIndices[cell]
        c = colIndices[cell]
        rowNeighbors = cellIndices[(rowIndices==r) & (np.abs(colIndices-c)==1)]
        colNeighbors = cellIndices[(colIndices==c) & (np.abs(rowIndices-r)==1)]
        AdjMatrix[cell,rowNeighbors] = 1
        AdjMatrix[cell,colNeighbors] = 1
    AdjMatrixT = np.triu(AdjMatrix)
    Indices = np.array(np.where(AdjMatrixT==1))
    Offset = np.sum(cells_per_layer[0:layer])
    Indices += Offset
    EdgeList = list(zip(Indices[0,:],Indices[1,:]))
    EdgeList = [[x,y] for (x,y) in EdgeList]
    return(EdgeList)

# Compute NodeList
NumLayers = len(Layering)
NumBridgeLayers = len(BridgeLayerIndices)
Module1Layers = Layering[0:BridgeLayerIndices[0]]
NumModule1Nodes = np.sum([r*c for r,c in Module1Layers])
Module2Layers = Layering[(BridgeLayerIndices[-1]+1):NumLayers]
NumModule2Nodes = np.sum([r*c for r,c in Module2Layers])
BridgeLayers = Layering[BridgeLayerIndices[0]:(BridgeLayerIndices[-1]+1)]
NumBridgeNodes = np.sum([r*c for r,c in BridgeLayers])
NumTotalNodes = NumModule1Nodes + NumBridgeNodes + NumModule2Nodes

Module1Nodes = list(range(0,NumModule1Nodes))
Module2Nodes = list(range(NumModule1Nodes+NumBridgeNodes,NumTotalNodes))
BridgeNodes = list(range(NumModule1Nodes,NumModule1Nodes+NumBridgeNodes))
NodeList = [Module1Nodes,Module2Nodes]

# Compute EdgeList
LatticeLayer = BridgeLayerIndices[0]
LatticeEdges = computeLatticeEdgeList(LatticeLayer)
LatticeNodes = list(np.unique(LatticeEdges))
LearnNodeList = LatticeNodes.copy()

# Adaptor edges: Single edge from Module1 last layer to adaptor, lattice adaptor, adaptor to Module2 layer 1
LearnEdgeList = [[Module1Nodes[-1],LatticeNodes[0]]]
LearnEdgeList.extend(LatticeEdges)
Module2Layer1Nodes = list(range(Module2Nodes[0],(Module2Nodes[0]+cells_per_layer[BridgeLayerIndices[-1]+1])))
LearnEdgeList.extend([[x,y] for x in LatticeNodes[1:] for y in Module2Layer1Nodes])  # the first lattice node is not part of the last adaptor layer
LearnEdgeList = np.array(LearnEdgeList)

NumLearnNodes = len(LearnNodeList)
InputCells = list(range(cells_per_layer[0]))
OutputCells = list([LearnNodeList[0]-1, LearnNodeList[NumLearnNodes-1]+1])  # upper and lower sockets

# Network parameters
CreateNetwork = True  # true when there is no interaction with GA, for example, while using backpropagation only
LoadNetwork = False  # because network is created by the GA module
SequentialInputs = True  # should we have this as an argument??
InputSequenceLength = 10
individual = 0
NetworkParams = []
StartTime = time.time()

# Mode=LearnPartial; true if LearNodeList and LearnEdgeList do not cover the full network

# Simulation arguments
BlockNodes = []

arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
             NumSimIters,SimParamFiles,NodeList,\
             LearnNodeList,LearnEdgeList,\
             Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
             dgj_scale,vm_scale,time_step,\
             SaveFile,SaveFileVersion,JobId,\
             RecordVisualData,\
             Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
             individual,StartTime,WallTime,\
             BlockNodes]

simBEN = SimBEN(arguments)
simBEN.MasterLearn()

