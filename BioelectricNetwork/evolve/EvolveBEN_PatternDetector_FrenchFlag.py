import sys
import ast
import numpy as np
from BioelectricNetwork.evolve.GA import GA
import torch
import time
import os
import glob

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd 
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# GA parameters
PopSize = int(args[16])
MutationRate = ast.literal_eval(args[17])
NumEpochs = int(args[18])  # NOTE: presently, this is not in use
# Storage parameters
SimParamFiles = args[19]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[20]
SaveFileVersion = args[21]
JobId = args[22]
RecordVisualData = ast.literal_eval(args[23])
WallTime = args[24]
epoch = int(args[25])

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(cells_per_layer)

def PerturbPattern(pattern,delta,group):
    num_pixels = len(pattern)
    if (group == 'live'):
        delta_index_range = np.arange(-delta,delta+1)
        perturbation = np.random.choice(delta_index_range,num_pixels)
        new_indices = (pattern + perturbation)
        new_indices = np.maximum(new_indices,0)
        new_indices = np.minimum(new_indices,num_polarity_levels-1)
    elif (group == 'dead'):
        delta_index_range = np.append(np.arange(-num_polarity_levels+1,-delta+1), np.arange(delta,num_polarity_levels))
        perturbation = np.random.choice(delta_index_range,num_pixels)
        new_indices = (pattern + perturbation) % num_polarity_levels  # assumes a wrapped vector of polarity indices 
        new_delta = new_indices - pattern
        new_delta_sign = np.sign(new_delta)
        new_abs_delta = abs(new_delta)
        new_delta = np.maximum(new_abs_delta, delta)*new_delta_sign
        new_indices = pattern + new_delta  
    return(list(new_indices))  # turn np array into a regular list

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)

InputLayerNumRows,InputLayerNumCols = Layering[0]
BasicPattern = [low,mid,high]
BasicPatternLen = len(BasicPattern)
nreps = int(BasicPatternLen*(InputLayerNumRows*InputLayerNumCols)/(BasicPatternLen*BasicPatternLen))
Main_pattern = np.repeat(BasicPattern,nreps)

Num_samples = 40
Inputs = [list(Main_pattern)]  # Note that "Inputs" contains indices into "Polarity_levels", not the polarities themselves
Outputs = [[high]]  # first item corresponds to the main pattern
Num_correct_samples = Num_incorrect_samples = int(Num_samples/2)  
for i in range(Num_correct_samples - 1):  # Note the main pattern, not included here, is a correct sample
    Inputs.append(PerturbPattern(Main_pattern,3,"live"))
    Outputs.append([high])  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
for i in range(Num_incorrect_samples):
    Inputs.append(PerturbPattern(Main_pattern,5,"dead"))
    Outputs.append([low])  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list

InputCells = list(range(np.prod(Layering[0])))
# OutputCells = InputCells.copy()
NumCells = int(np.sum([np.prod(x) for x in Layering]))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))

# Other GA parameters
LatticeLayers = [1]
LatticeDims = Layering[1]
ConstructLatticeLayer = True
AllowIntraLyerConnections = True
InterLayerConnectionDensity= 0.5

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA
LoadNetwork = True  # because network is created by the GA module
NodeList = []
SequentialInputs = True
InputSequenceLength = 5

def ClearFiles():
    Fnames = './Data/PatternDetectEvolveFrench_*.dat'
    fnames = glob.glob(Fnames)
    for fname in fnames:
        if os.path.isfile(fname):
            os.remove(fname)
    fname = './Data/GAResults' + SaveFileVersion + '.dat'
    if os.path.isfile(fname):
            os.remove(fname)
    fname = './Data/BENArguments' + SaveFileVersion + '.dat'
    if os.path.isfile(fname):
            os.remove(fname)

def ClearBENFiles():
    for individual in range(PopSize):
        fname = './Data/PatternDetectEvolveFrench_' + str(individual) + '_' + SaveFileVersion +'.dat'
        if os.path.isfile(fname):
            os.remove(fname)

def SaveResults(Population,Scores):
    WeightGenotype, BiasGenotype = Population
    NewResults = [WeightGenotype, BiasGenotype, Scores]
    ResultsFile = './Data/GAResults' + SaveFileVersion + '.dat'
    if os.path.isfile(ResultsFile):
        Results = torch.load(ResultsFile)
    else:
        Results = []
    Results.append(NewResults)
    torch.save(Results,ResultsFile)

def RetrieveBackproppedPopulation(ga):
    Scores = torch.FloatTensor([-99]*PopSize)
    WeightGenotype = torch.FloatTensor(np.random.uniform(-1,1,(num_cells,PopSize*num_cells)))
    BiasGenotype = torch.FloatTensor(np.zeros((PopSize*num_cells)))
    for individual in range(PopSize):
        fname = './Data/PatternDetectEvolveFrench_' + str(individual) + '_' + SaveFileVersion +'.dat'
        try:
            _,_,_,_,_,_,BestWeights,BestBias,BestWeightMatrix,_,BestErrors,_ = torch.load(fname)
        except:
            print('File ', fname, ' does not exist.')
            raise
            # continue  # NOTE: without 'continue', the loop will break by default
        else:
            # Update population with the best parameters obtained from backpropagation
            st1 = 0
            nd1 = num_cells
            st2 = individual*num_cells
            nd2 = st2 + num_cells
            BestW = BestWeightMatrix.clone()
            BestB = BestBias.clone()
            WeightGenotype[st1:nd1,st2:nd2] = BestW
            BiasGenotype[st2:nd2] = BestB
            Scores[individual] = BestErrors[-1]
    ga.Population = [WeightGenotype,BiasGenotype]
    return(Scores)

def GenerateBENArguments(Population):
    WeightGenotype, BiasGenotype = Population
    BENArguments = []
    for individual in range(PopSize):
        st1 = 0
        nd1 = num_cells
        st2 = individual*num_cells
        nd2 = st2 + num_cells
        W = WeightGenotype[st1:nd1,st2:nd2].clone()
        B = BiasGenotype[st2:nd2].clone() 
        NetworkParams = [W,B]
        LearnNodeList = []
        LearnEdgeList = []
        BlockNodes= []
        EvaluationProp = 0.2
        arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
                     NumSimIters,SimParamFiles,NodeList,\
                     LearnNodeList,LearnEdgeList,\
                     Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
                     dgj_scale,vm_scale,time_step,EvaluationProp,\
                     SaveFile,SaveFileVersion,JobId,\
                     RecordVisualData,\
                     Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
                     individual,StartTime,WallTime,\
                     BlockNodes]
        BENArguments.append(arguments)
    BENFile = './Data/BENArguments' + SaveFileVersion + '.dat'
    torch.save(BENArguments,BENFile)

if __name__ == '__main__':
    StartTime = time.time()
    ga_args = [PopSize,Layering,LatticeDims,MutationRate,ConstructLatticeLayer,AllowIntraLyerConnections,InterLayerConnectionDensity]
    ga = GA(ga_args)
    print('epoch = ',epoch)
    if epoch == 0:   # Generate population
        ClearFiles()
        ga.GeneratePopulation() 
    else:   # create new generation with new network structures and parameters: new layering, new AdjMatrix for the reservoir, new NetworkParams etc.
        Scores = RetrieveBackproppedPopulation(ga)  # load results file from the SimBEN processes
        SaveResults(ga.Population,Scores)
        ga.MicrobialTournament(Scores)
    GenerateBENArguments(ga.Population)  
    ClearBENFiles()


