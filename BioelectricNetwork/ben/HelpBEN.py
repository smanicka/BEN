from BioelectricNetwork.ben.BEN import BEN
import torch
import numpy as np
import itertools as itr

class HelpBEN():

    def __init__(self):
        pass

    def InitSysVars(self,benobj,num_polarity_levels):  # benobj has a separate InitSysVars()
        benobj.cc_cells = torch.FloatTensor([]) # array of arrays holding intracellular concentrations of all ions (mol/m3)
        benobj.Dm_array = torch.FloatTensor([]) # array of arrays holding membrane permeability for each ion
        for i, ion_obj in enumerate(benobj.ions_vect):
            benobj.cc_cells = torch.cat((benobj.cc_cells,torch.FloatTensor(ion_obj['c_in']*np.ones(1))))  # initial cell concentrations 
            benobj.Dm_array = torch.cat((benobj.Dm_array,torch.FloatTensor(ion_obj['D_mem']*np.ones(1))))  # removed the [] inside torch.FloatTensor()
        benobj.cc_cells = benobj.cc_cells.repeat(num_polarity_levels).view(num_polarity_levels,benobj.num_ions,1)  # repeat-then-view ensures samples are indexed by the depth (0th) dimension
        benobj.Dm_array = benobj.Dm_array.repeat(num_polarity_levels).view(num_polarity_levels,benobj.num_ions,1)

    # Compute ion channel Dm tuned for a given continuous ligand concentration level defined as follows.
    # Depolarization: SignalLevel = 1 => max K+ Dm, min Na+ Dm
    # Hyperpolarization: SignalLevel = 0  => min K+ Dm, max Na+ Dm
    # Intermediate levels of polarization: linear interpolation of the extreme levels above
    # Note that the slope and bias are explicitly set to 10 and 0.5 in order to facilitate a unique response to a wide range of signal concentrations;
    # large values of slope (e.g., m = 100) would result in unique sensitivity to only about [0.45,0.55] of the values of the signal concentration.
    def ComputeSingleCellConcVolt(self,Polarity_levels,num_polarity_levels):
        self.LayeringSingle = [[1,num_polarity_levels]]
        self.benSingleCell = BEN(self.LayeringSingle)
        self.benSingleCell.Inputs = [list(range(len(Polarity_levels)))]
        self.InitSysVars(self.benSingleCell,num_polarity_levels)
        self.benSingleCell.Dc_prop = torch.FloatTensor(Polarity_levels).view(num_polarity_levels,1)
        self.benSingleCell.Na_Dc = self.benSingleCell.Dc_prop*self.benSingleCell.Dm_max
        self.benSingleCell.K_Dc = (1-self.benSingleCell.Dc_prop)*self.benSingleCell.Dm_max
        self.benSingleCell.Dm_array[:,self.benSingleCell.ion_i['K'],:] = self.benSingleCell.K_Dc
        self.benSingleCell.Dm_array[:,self.benSingleCell.ion_i['Na'],:] = self.benSingleCell.Na_Dc
        self.benSingleCell.z_batch = self.benSingleCell.z_array.clone().repeat(num_polarity_levels,1,1)  # new shape = (num_polarity_levels,m,1)
        self.benSingleCell.InitVolts = torch.FloatTensor([0.0]*num_polarity_levels).view(num_polarity_levels,1)  # num_Cells=1
        self.benSingleCell.V_batch = self.benSingleCell.InitVolts.clone().unsqueeze(1)
        for t in range(5000):  # Turns out this many iterations is sufficient
            self.benSingleCell.ComputePassiveFlux(self.benSingleCell.V_batch,self.benSingleCell.z_batch)
            self.benSingleCell.Flux = torch.zeros(num_polarity_levels,self.benSingleCell.num_ions,1)
            self.benSingleCell.Flux = self.benSingleCell.Flux + self.benSingleCell.PassiveFlux
            self.benSingleCell.ComputeActiveFlux()
            self.benSingleCell.ActiveFlux = torch.zeros(num_polarity_levels,self.benSingleCell.num_ions,self.benSingleCell.num_cells)
            self.benSingleCell.ActiveFlux[:,self.benSingleCell.ion_i['Na'],:] = self.benSingleCell.NaFlux
            self.benSingleCell.ActiveFlux[:,self.benSingleCell.ion_i['K'],:] = self.benSingleCell.KFlux
            self.benSingleCell.Flux = self.benSingleCell.Flux + self.benSingleCell.ActiveFlux
            self.benSingleCell.cc_cells = self.benSingleCell.cc_cells + self.benSingleCell.Flux*(self.benSingleCell.cell_sa/self.benSingleCell.cell_vol)*self.benSingleCell.time_step
            self.benSingleCell.rho = torch.sum(self.benSingleCell.cc_cells*self.benSingleCell.z_batch,dim=1,keepdim=False)*self.benSingleCell.F  # keepdim = False would result in rho.shape=(num_polarity_levels,num_cells) rather than (num_polarity_levels,num_ions,num_cells)
            self.benSingleCell.InitVolts = (1/self.benSingleCell.cm)*self.benSingleCell.rho*(self.benSingleCell.cell_vol/self.benSingleCell.cell_sa)  # new shape = (num_polarity_levels,num_polarity_levels)
            self.benSingleCell.V_batch = self.benSingleCell.InitVolts.clone().unsqueeze(1)
        self.benSingleCell.InitVolts = self.benSingleCell.InitVolts[:,0].view(-1,1)  # new shape = (num_polarity_levels,1)
        self.benSingleCell.InitNa_Dm = self.benSingleCell.Dm_array[:,self.benSingleCell.ion_i['Na'],:]
        self.benSingleCell.InitK_Dm = self.benSingleCell.Dm_array[:,self.benSingleCell.ion_i['K'],:]

    def LoadParameterFile(self,benobj,nodesList,fname,LearnPartial=False):
        contents = torch.load(fname)
        if (len(contents)==12):
            _, _, _, _, _, _, BestWeights, BestBias, _, _, _, _ = torch.load(fname)  # this is the latest format
        elif (len(contents)==10):
            _, _, _, _, _, BestWeights, BestBias, _, _, _ = torch.load(fname)  # older format
        else:
            raise Exception('Parameter file is not of expected format')
        if LearnPartial:
            # 'BaseAdj' and 'n' would have been initialized either by loadNetworkStructureParams() using supplied 'NetworkParams' or initNetworkStructure() using 'Layering' and Connectivity scheme
            W = torch.ones(benobj.n*benobj.n).view(benobj.n,benobj.n)  
            W = W * torch.triu(benobj.BaseAdj)
            nodes = nodesList[0]  # since only one parameter file is loaded
            indices = np.array(list(itr.combinations(nodes,2)))
            W_local = torch.zeros(benobj.n*benobj.n).view(benobj.n,benobj.n)
            W_local[indices[:,0],indices[:,1]] = 1
            W_local = W_local * W
            x, y = np.where(W_local==1)
            W_local[x,y] = BestWeights.clone()
            benobj.W = benobj.W + W_local
            benobj.B[nodes] = BestBias.clone()
            benobj.W = benobj.W + benobj.W.t()
        else:
            benobj.Weights.data = BestWeights
            benobj.Bias.data = BestBias

    def LoadMultipleParameterFiles(self,benobj,nodesList,fnameList,LearnPartial=False):
        if LearnPartial:  # the parameters 'Weights' and 'Bias' contain only the values for the Bridge, which are randomly initialized in SimBEN
            benobj.W = torch.zeros(benobj.n*benobj.n).view(benobj.n,benobj.n)
            W = torch.ones(benobj.n*benobj.n).view(benobj.n,benobj.n)
            W = W * torch.triu(benobj.BaseAdj)
            benobj.B = torch.zeros(benobj.n)
            for i in range(len(fnameList)):
                fname = fnameList[i]
                contents = torch.load(fname)
                if (len(contents)==12):
                    _, _, _, _, _, _, BestWeights, BestBias, _, _, _, _ = torch.load(fname)  # this is the latest format
                elif (len(contents)==10):
                    _, _, _, _, _, BestWeights, BestBias, _, _, _ = torch.load(fname)  # older format
                else:
                    raise Exception('Parameter file is not of expected format')
                nodes = nodesList[i]
                indices = np.array(list(itr.combinations(nodes,2)))
                W_local = torch.zeros(benobj.n*benobj.n).view(benobj.n,benobj.n)
                W_local[indices[:,0],indices[:,1]] = 1
                W_local = W_local * W
                x, y = np.where(W_local==1)
                W_local[x,y] = BestWeights.clone()
                benobj.W = benobj.W + W_local
                benobj.B[nodes] = BestBias.clone()
            benobj.W = benobj.W + benobj.W.t()
        else:  # the parameters 'Weights' and 'Bias' are created and initialized with the values from the parameter files
            W_compound = torch.zeros(benobj.n*benobj.n).view(benobj.n,benobj.n)
            benobj.B = torch.zeros(benobj.n)
            for i in range(len(fnameList)):
                fname = fnameList[i]
                contents = torch.load(fname)
                if (len(contents)==12):
                    _, _, _, _, _, BestWeights, BestBias, BestW, _, _, _ = torch.load(fname)  # this is the latest format
                elif (len(contents)==10):
                    _, _, _, _, _, BestWeights, BestBias, _, _, _ = torch.load(fname)  # older format
                else:
                    raise Exception('Parameter file is not of expected format')
                nodes = nodesList[i]
                nodes = np.array(nodes)
                nodesForWeights = np.abs(nodes)  # turn negative values to positive
                W_local = torch.zeros(benobj.n*benobj.n).view(benobj.n,benobj.n)
                st,nd = nodesForWeights[0],nodesForWeights[-1]+1
                if (BestW.shape[0] > (nd-st)):
                    W_local[st:nd,st:nd] = torch.triu(BestW[st:nd,st:nd])
                else:
                    W_local[st:nd,st:nd] = torch.triu(BestW)
                W_compound = W_compound + W_local
                nodesForBias = np.delete(nodes,np.where(nodes<0))  # remove negative-valued nodes
                benobj.B[nodesForBias] = BestBias.clone()
            x, y = np.where(W_compound!=0)
            benobj.Weights.data = W_compound[x,y]
            benobj.Bias.data = benobj.B.clone()
            benobj.BaseAdj = W_compound.clone()
            benobj.BaseAdj[benobj.BaseAdj!=0] = 1
            benobj.BaseAdj += benobj.BaseAdj.t()
            benobj.NumEdges = int(torch.sum(benobj.BaseAdj)/2)

    def InitializeBEN(self,benobj,Inputs,Polarity_levels,num_polarity_levels,LearnPartial):
        benobj.InitSysVars(Inputs,Polarity_levels,num_polarity_levels,self.benSingleCell.InitVolts)
        benobj.initNetworkVars(LearnPartial)

    def UpdateVariables(self,benobj,LearnPartial):  # load parameters on to associated variables
        if LearnPartial:
            x, y = benobj.LearnEdgeList[:,0], benobj.LearnEdgeList[:,1]  # NOTE: not creating a new benobj.W out might affect the gradient propagation
            benobj.W[x,y] = benobj.Weights.clone()
            benobj.W[y,x] = benobj.Weights.clone()
            benobj.B[benobj.LearnNodeList] = benobj.Bias.clone()  # not cloning will result in an in-place operation error
        else:
            benobj.W = torch.ones(benobj.n*benobj.n).view(benobj.n,benobj.n)  # NOTE: Do we need to create a bew benobj.W?
            benobj.W = benobj.W * torch.triu(benobj.BaseAdj)
            x, y = np.where(benobj.W==1)
            benobj.B = benobj.Bias.clone()  # not cloning will result in an in-place operation error
            benobj.W[x,y] = benobj.Weights.clone()  # not cloning will result in an in-place operation error
            benobj.W = benobj.W + benobj.W.t()
        benobj.AbsW = torch.abs(benobj.W)
        benobj.NetW = torch.sum(benobj.AbsW,dim=1)  # row sum; shape = n (NOT 1xn or nx1)
        benobj.WFlat = benobj.W.view(1,-1)
        benobj.Adj = benobj.W.clone()  # not cloning will result in an in-place operation error
        benobj.Adj[benobj.Adj != 0] = 1

    def MapOutputVolts(self,simbenobj):
        simbenobj.OutputVolts = self.benSingleCell.InitVolts[simbenobj.Outputs,0].unsqueeze(0).unsqueeze(0)  # shapes of each item must be 3-dimensional

    def DisableNodes(self,benobj,nodes):
        for node in nodes:
            benobj.W[node,:] = 0
            benobj.W[:,node] = 0
        benobj.AbsW = torch.abs(benobj.W)
        benobj.NetW = torch.sum(benobj.AbsW,dim=1)  # row sum; shape = n (NOT 1xn or nx1)
        benobj.NetW[benobj.NetW==0] = 1
        benobj.WFlat = benobj.W.view(1,-1)
        benobj.Adj = benobj.W.clone()  # not cloning will result in an in-place operation error
        benobj.Adj[benobj.Adj != 0] = 1

