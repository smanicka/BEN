# BioElectricNetwork

Python libraries for simulating, training and evolving minimal bioelectric networks

This software accompanies the paper **[Modeling somatic computation with non-neural bioelectric networks](https://www.nature.com/articles/s41598-019-54859-8)**. 

All the data referenced in the paper are located in the [data](https://gitlab.com/smanicka/BEN/tree/master/BioelectricNetwork/data) folder.

Run the following command to set up the 'BioelectricNetwork' package:
pip install -e . --user

Required packages: pytorch, numpy, matplotlib
