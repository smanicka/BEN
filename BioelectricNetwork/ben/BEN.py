import torch
from torch.autograd import Variable
import numpy as np
import math

# Parameters - 'Weights' and 'Bias'
# Variables - everything else

## Below is a list of tensor shapes; these will be helpful in understanding some of the matrix computations
# n = num_cells
# m = num_ions
# p = NumInputStates
# t = NumSimIters
# W = nxn
# Dm_array = pxmxn 
# Dgj_array = nxn 
# NetDgj_array  = nx1 
# Dgj_array = pxnxn  # GJ-gating
# NetDgj_array  = pxn  # GJ-gating
# DgjCoeff = pxnxn  # GJ-gating
# Wgj_array = nxn 
# cc_cells = pxmxn 
# cc_env = mx1 
# cc_sig = pxn 
# z_array = mx1 
# vm = pxn 
# vm_time = txpxn
# cc_sig_time = txpxn
# V_batch = px1xn
# z_batch = pxmx1
# Dgj_batch = pxnxn

class BEN():

    def __init__(self,Layering=[[1,1]],FullConnectivity=False,TopographicConnectivity=False,LatticeLayers=[0],dgj_scale=1.0,time_step=1.0):

        ## Physiological parameters
        self.F = 96485 # Faraday constant [C/mol]
        self.R = 8.314  # Gas constant [J/K*mol]
        self.kb = 1.3806e-23  # Boltzmann constant [m2 kg/ s2 K1]
        self.q = 1.602e-19    # electron charge [C]
        self.tm = 7.5e-9    # thickness of cell membrane [nm]
        self.cm = 0.05  # patch capacitance of membrane [F/m2]
        self.T = 310   # temperature in Kelvin
        self.cell_r = 5.0e-6   # radius of single cell
        self.gj_len = 100e-9 # distance between two gap-junction connected cells [m]
        self.cell_sa = (4*math.pi*self.cell_r**2) # cell surface area
        self.cell_vol = ((4/3)*math.pi*self.cell_r**3) # cell volume
        self.Dgj = 20.0e-18  # gap junction diffusion coefficient for all ions m2/s
        self.Dgj_base = dgj_scale * 1.0e-18/self.gj_len  # DIFFERENT FROM V2, V3
        self.Dm_max = 150.0e-18
        self.leakage = 2.0e-10  # leakage (outward diffusion) coefficient for each cell
        
        ## Simulation parameters
        self.time_step = time_step * 1.0e-2  # time step; increasing this to even 2.0e-2 could lead to instability given other cellular constants.
        # self.time_step_sig = 1.0e7 # time step for diffusion of the signaling molecule
        
        ## Ion properties; D_mem = membrane diffusion coefficient, c_in = concentration in the cell, c_out = environmental concentration
        Na_ion = {'Name':'Na', 'D_mem':1.0e-18, 'z':1, 'c_in':10.0, 'c_out':145}
        K_ion = {'Name':'K', 'D_mem':1.0e-18, 'z':1, 'c_in':125.0, 'c_out':5}
        P_ion = {'Name':'P', 'D_mem':0.0e-18, 'z':-1, 'c_in':135.0, 'c_out':150}
        # stack the above individual dictionaries into a list
        self.ions_vect = [Na_ion, K_ion, P_ion]
        # create a reverse dictionary, so we can map ion numerical index to its string name
        self.ion_i = {}
        for j, ioni in enumerate(self.ions_vect):    
            self.ion_i[ioni['Name']] = j
        self.num_ions = len(self.ions_vect)
        self.cc_env  = torch.FloatTensor([])  # array holding fixed environmental concentrations of all ions (mol/m3); column-vector by default
        self.z_array = torch.FloatTensor([]) # array holding charge for each ion; column-vector by default
        for i, ion_obj in enumerate(self.ions_vect):
            self.cc_env = torch.cat((self.cc_env,torch.FloatTensor([ion_obj['c_out']]))) # fixed environmental concentrations
            self.z_array = torch.cat((self.z_array,torch.FloatTensor([ion_obj['z']])))  # fixed charge of ions
        self.cc_env  = self.cc_env.view(-1,1)  # array holding fixed environmental concentrations of all ions (mol/m3); column-vector by default
        self.z_array = self.z_array.view(-1,1)
        
        ## Network properties
        # Network topology
        self.FullConnectivity = FullConnectivity  # Tradittional ANN feed-forward topology with all-all connections between layers
        self.TopographicConnectivity = TopographicConnectivity  # Akin to convolutional NNs: small clusters of spatially neighboring cells project to the next layer; clusters can overlap
        self.LatticeLayers = LatticeLayers
        self.Layering = Layering
        self.cells_per_layer = [np.prod(x) for x in self.Layering]
        self.num_layers = len(self.cells_per_layer)
        self.n = self.num_cells = int(np.sum(self.cells_per_layer)) # total number of cells in network
        self.AllCellIndices = list(range(self.num_cells))
        self.CellIndicesPerLayer = []
        Curr = 0
        for i in self.cells_per_layer:
            self.CellIndicesPerLayer.append(self.AllCellIndices[Curr:(Curr+i)])
            Curr += i
        
        # The following two lines are required for the more nonlinear version of simulate
        arr = torch.arange(0,self.n,dtype=torch.long)
        self.rowrepidx = arr.clone().view(-1,1).repeat(1,self.n).view(1,-1).squeeze(0)
        self.colrepidx = arr.clone().view(-1,1).repeat(self.n,1).view(1,-1).squeeze(0)

    def initNetworkStructure(self):

        def GetLayerPairs(list):
            for i in range(len(list)-1):
                yield list[i:i+2]
                
        def computeConnectionStride(nummincells,nummaxcells):
            numLinksPerNode = min(max(nummaxcells-1,2),3)  # number of links per node must be either 2 or 3
            if nummincells > 1:
                stride = np.ceil((nummaxcells-numLinksPerNode)/(nummincells-1))  # creates maximal spacing between adjacent bundles of edges
            else:
                if numLinksPerNode > nummaxcells:
                    numLinksPerNode = nummaxcells
                stride = 0
            return(numLinksPerNode,stride)

        def computeInterLayerAdjacencyMatrix(layer1,layer2,rowOrCol):  # computes topographic connectivity between an adjacent pair of layers
            numLayer1RowCols = self.Layering[layer1][rowOrCol]
            numLayer2RowCols = self.Layering[layer2][rowOrCol]
            minrowcollayer, maxrowcollayer = (layer1,layer2) if numLayer1RowCols < numLayer2RowCols else (layer2,layer1)
            numminrowcols = self.Layering[minrowcollayer][rowOrCol]
            nummaxrowcols = self.Layering[maxrowcollayer][rowOrCol]
            if self.FullConnectivity:
                numLinksPerNode, stride = nummaxrowcols, 0
            elif self.TopographicConnectivity:
                numLinksPerNode, stride = computeConnectionStride(numminrowcols,nummaxrowcols)
            AdjMatrix = np.zeros((numminrowcols,nummaxrowcols),dtype=np.int8)
            for rc in range(numminrowcols):
                start = int(rc*stride)
                end = start + numLinksPerNode
                end = min(nummaxrowcols+1, end)
                AdjMatrix[rc,start:end] = 1
            if (minrowcollayer >= maxrowcollayer):
                AdjMatrix = np.transpose(AdjMatrix)
            return(AdjMatrix)

        def computeIntraLayerAdjacencyMatrix(layer):  # lattice connectivity within a layer
            numrows = self.Layering[layer][0]
            numcols = self.Layering[layer][1]
            rowIndices = np.tile(np.arange(0,numrows),numcols)
            colIndices = np.repeat(np.arange(0,numcols),numrows)
            numCellsLayer = self.cells_per_layer[layer]
            cellIndices = np.arange(numCellsLayer)
            AdjMatrix = np.zeros((numCellsLayer,numCellsLayer),dtype=np.uint8)
            for cell in cellIndices:
                r = rowIndices[cell]
                c = colIndices[cell]
                rowNeighbors = cellIndices[(rowIndices==r) & (np.abs(colIndices-c)==1)]
                colNeighbors = cellIndices[(colIndices==c) & (np.abs(rowIndices-r)==1)]
                AdjMatrix[cell,rowNeighbors] = 1
                AdjMatrix[cell,colNeighbors] = 1
            return(AdjMatrix)

        # This is the base Adjacency matrix; the actual Adj might change during the experiment
        self.BaseAdj = np.zeros((self.num_cells,self.num_cells),dtype=np.uint8)

        # Compute full, topographic or partial connectivity between adjacent pairs of layers
        if self.FullConnectivity or self.TopographicConnectivity:
            # Compute lattice connectivity within each layer    
            for layer in self.LatticeLayers:
                AdjMatrix = computeIntraLayerAdjacencyMatrix(layer)
                start, end = self.CellIndicesPerLayer[layer][0], self.CellIndicesPerLayer[layer][-1]+1
                self.BaseAdj[start:end,start:end] = AdjMatrix
            # Computer inter-layer connectivity
            LayerPairs = GetLayerPairs(list(range(self.num_layers)))
            for layer1,layer2 in LayerPairs:
                RowAdjMatrix = computeInterLayerAdjacencyMatrix(layer1,layer2,0)
                RowAdjMatrix = np.tile(np.tile(RowAdjMatrix,(self.Layering[layer1][1],1)),(1,self.Layering[layer2][1]))
                ColAdjMatrix = computeInterLayerAdjacencyMatrix(layer1,layer2,1)
                ColAdjMatrix = np.repeat(np.repeat(ColAdjMatrix,self.Layering[layer1][0],axis=0),self.Layering[layer2][0],axis=1)
                AdjMatrix = (RowAdjMatrix & ColAdjMatrix)
                start1,end1 = self.CellIndicesPerLayer[layer1][0], self.CellIndicesPerLayer[layer1][-1]+1
                start2,end2 = self.CellIndicesPerLayer[layer2][0], self.CellIndicesPerLayer[layer2][-1]+1
                self.BaseAdj[start1:end1,start2:end2] = AdjMatrix
                self.BaseAdj[start2:end2,start1:end1] = np.transpose(AdjMatrix)

        self.BaseAdj = torch.FloatTensor(self.BaseAdj)
        self.NumEdges = int(torch.sum(self.BaseAdj)/2)  # this will be an integer, assuming there are no self-loops in BaseAdj

    def loadNetworkStructureParams(self,NetworkParams,LearnFull=False,LearnPartial=False):
        W = NetworkParams[0].clone()  # already ensured to be symmetric
        # if FullConnectivity or TopographicConnectivity, then BaseAdj and NumEdges would be created by initNetworkStructure()
        self.BaseAdj = W.clone()
        self.BaseAdj[self.BaseAdj != 0] = 1
        self.NumEdges = int(torch.sum(self.BaseAdj)/2)  # this will be an integer, assuming there are no self-loops in BaseAdj
        #NOTE: We assume that NumLearnEdges and NumLearNodes are equal to NumEdges and NumNodes, and are set elsewhere, by a GA, for example
        if LearnFull or LearnPartial:
            self.Weights = Variable(torch.FloatTensor(np.zeros(self.NumEdges)),requires_grad=True)  # for the more nonlinear version of simulate
            self.Bias = Variable(torch.FloatTensor(np.zeros(self.n)),requires_grad=True)  # for the more nonlinear version of simulate
        else:
            self.Weights = torch.FloatTensor(np.zeros(self.NumEdges))
            self.Bias = torch.FloatTensor(np.zeros(self.n))
        x, y = np.where(self.BaseAdj.triu()==1)
        self.Weights.data = W[x,y].clone()
        self.Bias.data = NetworkParams[1].clone()

    def initNetworkVars(self,LearnPartial=False):
        if LearnPartial:
            x, y = self.LearnEdgeList[:,0], self.LearnEdgeList[:,1]  # NOTE: not creating a new benobj.W out might affect the gradient propagation
            self.W[x,y] = self.Weights.clone()
            self.W[y,x] = self.Weights.clone()
            self.B[self.LearnNodeList] = self.Bias.clone()  # not cloning will result in an in-place operation error
        else:
            # Set Weights, Bias and Adj
            self.W = torch.ones(self.n*self.n).view(self.n,self.n)  # NOTE: Do we need to create a bew benobj.W?
            self.W = self.W * torch.triu(self.BaseAdj)
            x, y = np.where(self.W==1)
            self.B = self.Bias.clone()
            self.W[x,y] = self.Weights.clone()
            self.W = self.W + self.W.t()
        self.AbsW = torch.abs(self.W)
        self.NetW = torch.sum(self.AbsW,dim=1)  # row sum; shape = n (NOT 1xn or nx1)
        self.WFlat = self.W.view(1,-1)  # needed for the more nonlinear version of simulate
        # Actual adjacency matrix
        self.Adj = self.W.clone()
        self.Adj[self.Adj != 0] = 1

    # Initialize variables
    def InitSysVars(self,Inputs,Polarity_levels,num_polarity_levels,InitVolts):
        self.NumInputStates = len(Inputs)
        self.cc_cells = torch.FloatTensor([]) # array of arrays holding intracellular concentrations of all ions (mol/m3)
        self.Dm_array = torch.FloatTensor([]) # array of arrays holding membrane permeability for each ion
        for i, ion_obj in enumerate(self.ions_vect):
            self.cc_cells = torch.cat((self.cc_cells,torch.FloatTensor(ion_obj['c_in']*np.ones(self.num_cells))))  # initial cell concentrations 
            self.Dm_array = torch.cat((self.Dm_array,torch.FloatTensor(ion_obj['D_mem']*np.ones(self.num_cells))))  # removed the [] inside torch.FloatTensor()
        self.cc_cells = self.cc_cells.repeat(self.NumInputStates).view(self.NumInputStates,self.num_ions,self.num_cells)  # repeat-then-view ensures samples are indexed by the depth (0th) dimension
        self.Dm_array = self.Dm_array.repeat(self.NumInputStates).view(self.NumInputStates,self.num_ions,self.num_cells)
        self.Dm_array_orig = self.Dm_array.clone()
        low = 0
        high = num_polarity_levels - 1
        mid = int((high + low)/2)
        MidPolarityLevel = Polarity_levels[mid]  # this is expected to be equal to 0.5
        self.init_cc_sig = torch.FloatTensor([MidPolarityLevel]).repeat(self.NumInputStates,self.n)
        self.cc_sig = self.init_cc_sig.clone()
        self.vm = torch.FloatTensor([InitVolts[mid,0]]*self.NumInputStates*self.num_cells).view(self.NumInputStates,self.num_cells)  # this is expected to be close to 0.0

    # Initialize input layer.
    # Depending on the given polarization (H or D) for the input cells, set the appropriate ion concentrations.
    # The ion concentrations, and the respective voltages, corresponding to H and D are pre-computed and stored in 'InitCons'.
    def InitInputLayer(self,InputCells,Inputs,Polarity_levels,InitNa_Dm,InitK_Dm,InitConcs,InitInputVolts):
        self.cc_sig[:,InputCells] = torch.FloatTensor([Polarity_levels[i] for i in Inputs])  # set input layer
        # Shapes: Dm_array=(p,m,n); Dm_array[:,ion_i['K'],:]=(p,n); InitK_Dm=(num_polarity_levels,1); InitK_Dm[Inputs,:]=(p,NumTargetCells,1); InitK_Dm[Inputs,0]=(p,NumTargetCells)
        # NOTE: The below line was included after backward() caused in-place operation error in pytorch1.0.1 but not 0.4.1.
        # ALSO NOTE: In the earlier version, we were not resetting the P ion concentrations, but by cloning Dm_array_orig, we now do.
        # Therefore, if you test param files that were obtained from previous versions of this code, then there is a chance that they won't produce the expected results.
        self.Dm_array = self.Dm_array_orig.clone()
        self.Dm_array[:,self.ion_i['Na'],InputCells] = InitNa_Dm[Inputs,0] 
        self.Dm_array[:,self.ion_i['K'],InputCells] = InitK_Dm[Inputs,0]
        # Shapes: cc_cells=(p,m,n); InitConcs=(num_polarity_levels,m,1); InitConcs[Inputs,:,0]=(p,NumTargetCells,m); InitConcs[Inputs,:,0].transpose_(1,2)=(p,m,NumTargetCells)
        self.cc_cells[:,:,InputCells] = InitConcs[Inputs,:,0].transpose(1,2)
        # Shapes: V=(p,n); InitVolts=(num_polarity_levels,1), InitVolts[l,0]=(p,NumTargetCells)
        # self.vm[:,InputCells] = InitVolts[Inputs,0]
        self.vm[:,InputCells] = InitInputVolts.clone()

    def UpdateSignalconcs(self):
        WScaled = self.W * self.DgjCoeff  # shape=(p,n,n); W=(n,n); DgjCoeff=(p,n,n)
        # self.DgjCoeff = torch.ones(self.NumInputStates*self.n*self.n).view(self.NumInputStates,self.n,self.n)  # turn off link between gj-gating and ligand flow
        # WScaled = self.W * self.DgjCoeff # shape=(p,n,n); W=(n,n); DgjCoeff=(p,n,n)
        NetWScaled = torch.sum(torch.abs(WScaled),dim=2)  # shape=(p,n)
        NetWScaled[NetWScaled==0] = 1
        WScaledFlat = WScaled.view(-1,self.n*self.n)  # this works as expected (verified using simple examples)
        InputFlux = -WScaledFlat * (self.cc_sig[:,self.colrepidx] - self.cc_sig[:,self.rowrepidx])  # shape=(p,n*n)
        SigmoidFlux = 1/(1+torch.exp(InputFlux))
        # SigmoidFlux = 1/(1+torch.exp(20*InputFlux))  # required for the less nonlinear version, or if Weights are restricted to [-1,1]  # CRITICAL FEATURE
        SigmoidFlux = SigmoidFlux.view(self.NumInputStates,self.num_cells,self.num_cells)  # shape=(p,n,n)
        # SigmoidFlux = SigmoidFlux * Adj * AbsW  # Adj ensures zero-flux through non-connections and weights it  #CRITICAL FEATURE
        SigmoidFlux = SigmoidFlux * self.Adj * self.AbsW * self.DgjCoeff  # Adj ensures zero-flux through non-connections and weights it  #CRITICAL FEATURE
        # NetSigmoidFlux = torch.sum(SigmoidFlux,dim=1)/NetW  # normalizes weights  #CRITICAL FEATURE
        NetSigmoidFlux = torch.sum(SigmoidFlux,dim=2)/NetWScaled  # both numerator and denominator shapes=(p,n); normalizes weights  #CRITICAL FEATURE
        ds = 1/(1+torch.exp(-20*(NetSigmoidFlux-self.B)))  
        ds = 2*ds - 1  # to map [0,1] to [-1,1]
        self.cc_sig = self.cc_sig + ds*0.02  #IMPORTANT FEATURE; 0.02 is the maximum velocity
        self.cc_sig[self.cc_sig < 0] = 0
        self.cc_sig[self.cc_sig > 1] = 1

    def UpdateIonChannelDc(self):
        Dc_prop = self.cc_sig.clone()  # clone() is important here, or else gradients will be affected  # all the nonlinearity goes into determining cc_sig itself
        Na_Dc = Dc_prop*self.Dm_max
        K_Dc = (1-Dc_prop)*self.Dm_max
        # NOTE: Avoid in-place operations like the following - they don't work well with backpropagation
        # self.Dm_array[:,0,:] = Na_Dc#.clone()
        # self.Dm_array[:,1,:] = K_Dc#.clone()
        self.Dm_array = torch.stack((Na_Dc,K_Dc,self.Dm_array_orig[:,self.ion_i['P'],:]),dim=1)  # this is required to correctly pass down the requires_grad flag 

    # Update passive membrane ion flux for each cell based on the GHK flux electrodiffusion equations.
    # If V = 0 for a cell, then ion flux is determined by just diffusion.
    def ComputePassiveFlux(self,V_batch,z_batch):
        alpha = torch.bmm(z_batch,V_batch) * self.F/(self.R*self.T)
        # NOTE: einsum has a bug in 0.4.0 which prevents backward() from functioning correctly (throws error "one of the variables needed for gradient computation has been modified by an inplace operation")
        # alpha = torch.einsum('mq,pn->pmn',(z,V)) * p.F/(p.R*p.T)  # z*V_nz is a batch outer product of shape pxmxn, with the "batch dimension" being p; in 'mq', q=1
        ElectricComponent = torch.exp(-alpha)
        idx1 = (ElectricComponent >= (1 - 1.0e-10))
        idx2 = (ElectricComponent <= (1 + 1.0e-10))
        ElectricComponentClone = ElectricComponent.clone()  # NOTE that setting it to torch.exp(-alpha) then setting 0 in-place, or avoiding .clone() throws error in backward()
        ElectricComponentClone[idx1 * idx2] = 0.99  # replace values equal to 1 (because V=0) with 0 to avoid infinite denominator; logical & does not yet work with tensors
        self.PassiveFlux = -(alpha*(self.Dm_array/self.tm)*(self.cc_cells-(self.cc_env*ElectricComponentClone))/(1-ElectricComponentClone))

    # Update active membrane ion flux established by the ion pumps.
    # Note that the below is an approximation of the actual mechanism that involves ATP and others (detailed in BETSE). It is approximate to the
    # extent that it suffices to counterbalance the passive electrodiffusion in such a way that the result is a asymptotically stable voltage.
    # Basic idea: Normally there is more K_in than K_out, and more Na_out than Na_in. Thus, if intracellular Na increases or intracellular K decreases, 
    # then there is more K inflow, and more Na outlfow, thereby tending to counterbalance the diffusion part of electrodiffusion. Although remember
    # that the electrophoresis already counteracts the passive diffusion.
    def ComputeActiveFlux(self):
        Na_i = self.ion_i['Na']
        K_i = self.ion_i['K']
        self.KFlux = ((self.cc_cells[:,Na_i,:]/self.cc_env[Na_i])**3) * ((self.cc_env[K_i]/self.cc_cells[:,K_i,:])**2)
        self.NaFlux = -1.1*self.KFlux

    # Update gap junction (passive) ion flux based on discretized 1-dimensional Nernst-Planck equations.
    # Note: I tried to get rid of the transposes here but that would have introduced a couple in the passive flux namely cc.T and Flux.T; so left it as it is.
    def ComputeGapJuncFluxNP(self):
        ccT = self.cc_cells.transpose(1,2)  # shape: ccT=(p,n,m)
        NetDgj_batch = self.NetDgj_array.clone().unsqueeze(2)  # shape=(p,n,1)
        ConcFlux = (torch.bmm(self.Dgj_array,ccT)-(ccT*NetDgj_batch)).transpose(1,2)   # shape=(p,m,n); ccT*NetDgj shapes [(p,n,m),(n,1)] align and they multiply as expected (verified with toy examples)
        # shapes: V=(p,n); Dgj=(n,n); NetDgj=(n,1); z=(m,1); cc=(p,m,n); (cc*z)=(p,m,n)
        NetDgj_batch2 = self.NetDgj_array.clone().unsqueeze(1)  # shape=(p,1,n)
        VoltTerm = self.V_batch.bmm(self.Dgj_array.transpose(1,2))-(self.V_batch*NetDgj_batch2)  # shape: VoltTerm=(p,1,n); term1:(p,1,n).bmm(p,n,n)=(p,1,n); term2=(p,1,n)*(p,1,n)=(p,1,n)
        # VoltTerm = (V.mm(Dgj.t())-(V*NetDgj.t()))  # shape = (p,n)
        ConcTerm = (self.cc_cells*self.z_array)  # shape = (p,m,n)
        VoltFlux = (self.q/(self.kb*self.T))*VoltTerm*ConcTerm  # shape = (p,m,n)
        self.GJFlux = (ConcFlux + VoltFlux)  # shape=(p,m,n)

    def UpdateIonConcs(self,ChOn=True,GJOn=True):
        self.Flux = torch.zeros(self.NumInputStates,self.num_ions,self.num_cells)
        if ChOn:
            self.ComputePassiveFlux(self.V_batch,self.z_batch)
            self.Flux = self.Flux + self.PassiveFlux
            self.ComputeActiveFlux()
            self.ActiveFlux = torch.zeros(self.NumInputStates,self.num_ions,self.num_cells)
            self.ActiveFlux[:,self.ion_i['Na'],:] = self.NaFlux
            self.ActiveFlux[:,self.ion_i['K'],:] = self.KFlux
            self.Flux = self.Flux + self.ActiveFlux
        if GJOn:
            self.ComputeGapJuncFluxNP()
            self.Flux = self.Flux + self.GJFlux
        self.cc_cells = self.cc_cells + self.Flux*(self.cell_sa/self.cell_vol)*self.time_step

    def UpdateVmem(self,z_batch):
        rho = torch.sum(self.cc_cells.clone()*self.z_batch,dim=1,keepdim=False)*self.F
        # rho = torch.sum(self.cc_cells*self.z_batch,dim=1,keepdim=False)*self.F
        self.vm = (1/self.cm)*rho*(self.cell_vol/self.cell_sa)
