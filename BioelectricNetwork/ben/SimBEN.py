from BioelectricNetwork.ben.BEN import BEN
from BioelectricNetwork.ben.HelpBEN import HelpBEN
import torch
from torch.autograd import Variable
import numpy as np
import random
import time
import re

# torch-related notes:
# 1) Parameters: Weights and Bias. Thus, require_grad=True and is_leaf=True for both.
# 2) Variables: everything else, including the derivatives of Weights namely W, AbsW, etc.. For all of these, require_grad=True; for some of those is_leaf=True as well (not sure why)
# 3) If Parameter values need to be updated on-the-fly, then it's essential to clone them before updating, or else it will result in an "in-place operation error". Here, the clones are
#    implemented in HelpBEN.UpdateVariables().

class SimBEN():

    def __init__(self,arguments):  # these parameters depend on the experiment
        self.start_time = time.time()
        # Learning arguments
        self.NumLearningIters = arguments[0]
        self.Mode = arguments[1]
        self.CreateNetwork = arguments[2]
        self.LoadNetwork = arguments[3]
        self.Resume = arguments[4]
        self.UseMomentum = arguments[5]
        # Simulation arguments
        self.NumSimIters = arguments[6]
        self.SimParamFiles = ['./Data/'+f for f in arguments[7]]
        self.NodeList = arguments[8]
        # Special Learning arguments (e.g, train only a part of the network)
        self.LearnNodeList = arguments[9]
        self.LearnEdgeList = arguments[10]
        # Network arguments
        self.Layering = arguments[11]
        self.FullConnectivity = arguments[12]
        self.TopographicConnectivity = arguments[13]
        self.LatticeLayers = arguments[14]
        self.TurnOffGapJuncGating = arguments[15]
        self.DelayProp = arguments[16]
        self.ClampProp = arguments[17]
        self.NetworkParams = arguments[18]
        # Simulation arguments
        self.dgj_scale = arguments[19]
        self.vm_scale = arguments[20]
        self.time_step = arguments[21]
        self.eval_prop = arguments[22]
        self.NumEvalIters = int(np.ceil(self.eval_prop*self.NumSimIters[0]))
        # Storage arguments
        self.SaveFile = arguments[23]
        self.SaveFileVersion = arguments[24]
        self.JobId = arguments[25]
        # Visualization arguments
        self.RecordVisualData = arguments[26]
        # Initialization of BEN network
        self.ben = BEN(self.Layering,self.FullConnectivity,self.TopographicConnectivity,self.LatticeLayers,self.dgj_scale,self.time_step)
        self.LearnFull, self.LearnPartial, self.LearnNone, self.LearnHomogenous, self.ComputeSensitivity, self.TrackGrads = False, False, False, False, False, False
        if self.Mode == 'LearnFull':
            self.LearnFull = True
            self.TrackGrads = True
        elif self.Mode == 'LearnPartial':
            self.LearnPartial = True
            self.TrackGrads = True
        elif self.Mode == 'LearnHomogenous':
            self.LearnHomogenous = True
            self.TrackGrads = True
        elif self.Mode == 'LearnNone':  # equivalent to simulation mode but without a parameter file (e.g., parameters passed by a gA)
            self.LearnNone = True
        elif self.Mode == 'ComputeSensitivity':
            self.ComputeSensitivity = True
            self.TrackGrads = True
        if self.CreateNetwork:
            self.ben.initNetworkStructure()
        elif self.LoadNetwork:  # from a GA, for example, but not from parameter files which is done separately below
            self.ben.loadNetworkStructureParams(self.NetworkParams,self.LearnFull,self.LearnPartial)
        # Input and output arguments specific to this experiment
        self.Polarity_levels = arguments[27]
        self.InputsOrig = arguments[28]
        self.OutputsOrig = arguments[29]
        self.Inputs = self.InputsOrig.copy()
        self.Outputs = self.OutputsOrig.copy()
        self.SequentialInputs = arguments[30]
        self.InputSequenceLength = arguments[31]
        self.InputCells = arguments[32]
        self.NumInputCells = len(self.InputCells)
        self.OutputCells = arguments[33]
        # Parallel programming (Cluster) arguments
        self.IndividualNum = arguments[34]
        self.StartTime = arguments[35]
        self.WallTime = arguments[36]
        # Simulation arguments
        self.DisabledNodes = arguments[37]
        self.num_polarity_levels = len(self.Polarity_levels)
        self.NumInputStates = len(self.Inputs)
        self.benHelp = HelpBEN()
        self.benHelp.ComputeSingleCellConcVolt(self.Polarity_levels,self.num_polarity_levels)  # compute cc_sig, cc_cells, vm etc. for the full range of polarity levels
        self.benHelp.MapOutputVolts(self)
        if self.LearnFull or self.LearnNone or self.LearnHomogenous:  # LEARNING MODE
            self.NumIters = self.NumLearningIters
            if self.LearnNone:
                self.NumIters = 1
            self.BestError = 999
            self.BestErrors = []
            self.AllErrors = []
            self.weight_learning_rate = self.bias_learning_rate = 0.02
            if self.LearnHomogenous:
                self.NumLearnEdges = 1
                self.NumLearnNodes = 1
            else:
                self.NumLearnEdges = self.ben.NumEdges
                self.NumLearnNodes = self.ben.n
            self.previous_delta_Weights = torch.zeros(self.NumLearnEdges)
            self.previous_delta_Bias = torch.zeros(self.NumLearnNodes)
            if self.Resume:
                self.ben.Weights = Variable(torch.FloatTensor(np.zeros(self.NumLearnEdges)),requires_grad=True)  # for the more nonlinear version of simulate
                self.ben.Bias = Variable(torch.FloatTensor(np.zeros(self.NumLearnNodes)),requires_grad=True)  # for the more nonlinear version of simulate
                if (len(self.SimParamFiles) > 1):
                    self.benHelp.LoadMultipleParameterFiles(self.ben,self.NodeList,self.SimParamFiles)
                else:
                    self.benHelp.LoadParameterFile(self.ben,self.NodeList,self.SimParamFiles[0])
            elif not self.LoadNetwork:  # in LoadNetwork, then parameters will be loaded as well in BEN.loadNetworkStructureParams()
                self.ben.Weights = Variable(torch.FloatTensor(np.random.uniform(-1,1,self.NumLearnEdges)),requires_grad=True)  # for the more nonlinear version of simulate
                self.ben.Bias = Variable(torch.FloatTensor(np.random.uniform(0,1,self.NumLearnNodes)),requires_grad=True)  # for the more nonlinear version of simulate
            self.ben.InitWeights = self.ben.Weights.clone()
            self.ben.InitBias = self.ben.Bias.clone()
        elif self.LearnPartial:  # LEARNING MODE: should be the opposite of LearnFull, unless in simulation mode and not learning mode
            self.NumIters = self.NumLearningIters
            self.BestError = 999
            self.BestErrors = []
            self.AllErrors = []
            self.weight_learning_rate = self.bias_learning_rate = 0.02
            self.ben.LearnNodeList = self.LearnNodeList
            self.ben.LearnEdgeList = self.LearnEdgeList
            self.NumLearnEdges = len(self.ben.LearnEdgeList)
            self.NumLearnNodes = len(self.ben.LearnNodeList)
            # We assume that the bridge (or the "adaptor") has just two "sockets" - one node from the upper module and one in the lower module
            self.previous_delta_Weights = torch.zeros(self.NumLearnEdges)
            self.previous_delta_Bias = torch.zeros(self.NumLearnNodes)
            self.ben.Weights = Variable(torch.FloatTensor(np.random.uniform(-1,1,self.NumLearnEdges)),requires_grad=True)  # for the more nonlinear version of simulate
            self.ben.Bias = Variable(torch.FloatTensor(np.random.uniform(0,1,self.NumLearnNodes)),requires_grad=True)
            if (len(self.SimParamFiles) > 1):
                self.benHelp.LoadMultipleParameterFiles(self.ben,self.NodeList,self.SimParamFiles,True)  # these files load data only for the rest of the network
            else:
                self.benHelp.LoadParameterFile(self.ben,self.NodeList,self.SimParamFiles[0],True) # these files load data only for the rest of the network
            self.ben.InitWeights = self.ben.Weights.clone()
            self.ben.InitBias = self.ben.Bias.clone()
        else:  # SIMULATION MODE  # the parameters Weights and Bias are not needed in this mode
            self.NumIters = 1
            if not self.ComputeSensitivity:
                self.TrackGrads = False
            if (len(self.SimParamFiles) > 1):
                self.benHelp.LoadMultipleParameterFiles(self.ben,self.NodeList,self.SimParamFiles)
            else:
                self.benHelp.LoadParameterFile(self.ben,self.NodeList,self.SimParamFiles[0])
        # Initialization of BEN parameters and variables (including setting the input layer states)
        self.benHelp.InitializeBEN(self.ben,self.Inputs,self.Polarity_levels,self.num_polarity_levels,self.LearnPartial)
        self.ben.InitW = self.ben.W.clone()
        # Learning arguments
        self.max_delta_exp_weight = -1  # max delta of 0.w, where w = weight; that is max delta = 0.9 (not 0.1)
        self.max_delta_exp_bias = -2  # max delta of 0.0b, where b = bias; that is max delta = 0.09 (not 0.01)
        self.slope_learn = 2*(10**5)
        self.threshold_learn = 0.0002  # threshold error = (0.015^2) ~ 0.0002
        self.WallTime = np.array(list(map(int,re.split(':|-',self.WallTime))))
        TimeCoeffs = np.array([24*60*60,60*60,60,1])
        self.WallTimeSecs = np.sum(self.WallTime*TimeCoeffs)
        self.WallTimeThreshold = 0.98*self.WallTimeSecs

    # NOTE: 'TrackGrads' is required to keep track of gradients during some phases of the experiment but not in others.
    def Simulate_nonlinear(self,NumSimIters,RecordVisualData=False):    # NumSimIters is a local variable
        torch.set_grad_enabled(False)  # gradients will start getting accumulated only at the set time point below
        self.ben.vm_time = torch.FloatTensor([])
        self.ben.V_batch = self.ben.vm.clone().unsqueeze(1)  # new shape = (p,1,n); also if you don't .clone() then backward might not work (it nulls the requires_grad flag)
        self.ben.NetW[self.ben.NetW==0] = 1
        if RecordVisualData:
            self.ben.cc_sig_time = torch.FloatTensor([])
            self.ben.Dgj_time = torch.FloatTensor([])
            onesidx = (torch.triu(self.ben.BaseAdj)==1)
        ## Voltage gating of gap junction:
        # We assume that higher voltages make the GJ more permeable and lower voltages make them less permeable for all cells.
        # Note that the coefficient of 3 in the gating sigmoid was chosen empirically to ensure an optimal balance between the slope of the
        # sigmoid and the length of the range of outputs in the interval [0,1], given the fact that W is likely to lie in the range [-20,20] and
        # that V lies in [-0.08,0.08].
        if self.TurnOffGapJuncGating:
            self.ben.DgjCoeff = torch.ones(self.ben.NumInputStates*self.ben.n*self.ben.n).view(self.ben.NumInputStates,self.ben.n,self.ben.n)  # turn off gap junction gating
        else:
            self.ben.DgjCoeff = self.ben.Adj / (1+torch.exp(-3*self.ben.AbsW * self.ben.V_batch)) # Adj=(n,n); V_batch=(p,1,n); Dgj_coeff=(p,n,n); AbsW*V_batch auto-broadcasting works as expected
        self.ben.Dgj_array = self.ben.DgjCoeff * self.ben.AbsW * self.ben.Dgj_base  # shape=(p,n,n)
        self.ben.Dgj_array = (self.ben.Dgj_array + self.ben.Dgj_array.transpose(1,2))/2.0  # voltage-gating makes Dgj_array asymmetrical, so we reimpose symmetry
        self.ben.NetDgj_array = torch.sum(self.ben.Dgj_array,dim=2)  # shape=(p,n); Total sum of scaled Dgj per cell    z_batch = z_array.clone().repeat(NumInputStates,1,1)  # new shape = (p,m,1)
        self.ben.z_batch = self.ben.z_array.clone().repeat(self.NumInputStates,1,1)  # new shape = (p,m,1)
        # Note that now Dgj depends on V that in turn depends on each input sample, so the below line is commented and we pass Dgj_array instead of Dgj_batch below
        # Dgj_batch = Dgj_array.clone().repeat(NumInputStates,1,1)
        DelayLength = np.ceil(self.DelayProp*NumSimIters)
        ClampLength = np.ceil(self.ClampProp*(NumSimIters-DelayLength))
        TotalClampLength = ClampLength + DelayLength
        self.InitInputVm = self.benHelp.benSingleCell.InitVolts[self.Inputs,0]
        self.InitFullVm = self.ben.vm.detach()
        ClampInputs = False
        for t in np.arange(0,NumSimIters):
            if ((t==(NumSimIters-100)) & self.TrackGrads):
            # if ((t==0) & TrackGrads):
                torch.set_grad_enabled(True)
                if (self.LearnFull or self.LearnPartial or self.LearnHomogenous):
                    self.ben.Weights.requires_grad = True
                    self.ben.Bias.requires_grad = True
                    self.benHelp.UpdateVariables(self.ben,self.LearnPartial)  # this is an important step that ensures gradients are tracked in all downstream variables
                if self.ComputeSensitivity:  # initialize input vm and full vm parameters to keep track of gradients
                    self.InitInputVm.requires_grad = True
                    self.InitFullVm.requires_grad = True
                    self.ben.vm = self.InitFullVm.clone()  # vm starts getting updated starting t=0 regardless of anything else
            if ((t >= DelayLength) and (t <= TotalClampLength)):  # Delay inputs and Clamp inputs; # NOTE: if DelayLength=ClampLength=0, then input is applied just once
                # initialization of inputs should follow set_grad_enabled(True), else gradients with respect to initial states won't be tracked
                self.ben.InitInputLayer(self.InputCells,self.Inputs,self.Polarity_levels,self.benHelp.benSingleCell.InitNa_Dm,self.benHelp.benSingleCell.InitK_Dm,self.benHelp.benSingleCell.cc_cells,self.InitInputVm)
                self.benHelp.UpdateVariables(self.ben,self.LearnPartial)  # this is an important step that ensures gradients are tracked in all downstream variables
                self.ben.NetW[self.ben.NetW==0] = 1  # applies for blocked nodes
                ClampInputs = True
            else:
                ClampInputs = False
            if t==0:
                self.benHelp.DisableNodes(self.ben,self.DisabledNodes)
            self.ben.Dgj_array = self.ben.DgjCoeff * self.ben.AbsW * self.ben.Dgj_base  # shape=(p,n,n)
            self.ben.Dgj_array = (self.ben.Dgj_array + self.ben.Dgj_array.transpose(1,2))/2.0  # voltage-gating makes Dgj_array asymmetrical, so we reimpose symmetry
            self.ben.NetDgj_array = torch.sum(self.ben.Dgj_array,dim=2)  # shape=(p,n); Total sum of scaled Dgj per cell
            self.ben.vm_time = torch.cat((self.ben.vm_time, self.ben.vm))  # NOTE: since vm is collected at the start of the iteration, NumSimiters must be > 1 (and t at which grad is enabled must be less than NumSimiters), else gradients can't be computed
            self.ben.UpdateSignalconcs()
            self.ben.UpdateIonChannelDc()
            self.ben.UpdateIonConcs(ChOn=True,GJOn=True)
            self.ben.UpdateVmem(self.ben.z_batch)  # this step updates vm of the input cells as well, so reset them at the end of the iteration to their clamped states if applicable
            self.ben.V_batch = self.ben.vm.clone().unsqueeze(1)
            # NOTE: For some reason I had moved the below lines for updating DgjCoeff up to just after DisableNodes(), and that was sufficient to completely alter the behavior of some trained circuits
            if self.TurnOffGapJuncGating:
                self.ben.DgjCoeff = torch.ones(self.ben.NumInputStates*self.ben.n*self.ben.n).view(self.ben.NumInputStates,self.ben.n,self.ben.n)  # turn off gap junction gating
            else:
                self.ben.DgjCoeff = self.ben.Adj / (1+torch.exp(-3*self.ben.AbsW * self.ben.V_batch)) # Adj=(n,n); V_batch=(p,1,n); Dgj_coeff=(p,n,n); AbsW*V_batch auto-broadcasting works as expected
            if RecordVisualData:
                self.ben.cc_sig_time = torch.cat((self.ben.cc_sig_time,self.ben.cc_sig))
                self.ben.Dgj_time = torch.cat((self.ben.Dgj_time,torch.masked_select(self.ben.DgjCoeff,onesidx)/self.ben.Dgj_base))
        if ClampInputs:  # this step ensures that input cells are clamped at the end of the iteration as well, so that gradients with respect to input cells are calculate accurately
            self.ben.InitInputLayer(self.InputCells,self.Inputs,self.Polarity_levels,self.benHelp.benSingleCell.InitNa_Dm,self.benHelp.benSingleCell.InitK_Dm,self.benHelp.benSingleCell.cc_cells,self.InitInputVm)
            self.benHelp.UpdateVariables(self.ben,self.LearnPartial)  # this is an important step that ensures gradients are tracked in all downstream variables
            self.ben.NetW[self.ben.NetW==0] = 1  # applies for blocked nodes
        if RecordVisualData:
            self.ben.cc_sig_time = self.ben.cc_sig_time.view(NumSimIters,self.NumInputStates,self.ben.n)
            self.ben.Dgj_time = self.ben.Dgj_time.view(NumSimIters,self.ben.NumInputStates,self.ben.NumEdges)
        self.ben.vm_time = self.ben.vm_time.view(NumSimIters,self.NumInputStates,self.ben.n)

    def MSELoss(self,vm_list,Outputs):
        target_vm = Outputs
        target_vm = target_vm * self.vm_scale
        # print('Initial = ', vm_list[0], 'Final = ', vm_list[-1], ' target = ', target_vm)
        # print('Observed = ',vm_list[-10:], ' target = ',target_vm)
        # print('difference = ', vm_list[-10:] - target_vm)
        self.ben.err = (vm_list - target_vm).pow(2).mean()

    def ShuffleIndices(self,idx):
        mtch = False
        while not mtch:
            idx2 = idx.copy()
            random.shuffle(idx2)
            cmp = [i for i,j in zip(idx,idx2) if i==j]
            if (len(cmp) == 0):
                mtch = True
        return(idx2)

    def Backprop(self):
        self.ben.err.backward(retain_graph=True)
        self.TotalWGrad = self.TotalWGrad + self.ben.Weights.grad
        self.TotalBGrad = self.TotalBGrad + self.ben.Bias.grad
        torch.set_grad_enabled(False)
        self.ben.Weights.requires_grad = False
        self.ben.Bias.requires_grad = False
        self.ben.Weights.grad.data.zero_()
        self.ben.Bias.grad.data.zero_()

    # NOTE: Each step/phase in an experiment that requires an error calculation and a gradient following that needs to start "from scratch".
    # More specifically, every level-1 variable (parameters are level-0) must be redefined and reinitialized from the parameters, so that they
    # possess the right gradient flags. We do this for hygiene mostly, and not utter necessity. For example, in the "cut" step in Phase 2 below,
    # it's ok to not initialize using UpdateVariables() but it would be good to do so. This hygience step crucially depends on the assumption that
    # the level-1 variables don't change during the simulation step; these level-1 variables are the embodiments of the parameters which are not
    # used directly. In this program, there are two parameters namely 'Weights' and 'Bias'; their embodiments (level-1 variables) are respectively
    # 'W' and 'B'. Sometimes, we refer to 'W' as a meta-parameter because the actual weights (gap junction permeabilities encoded by 'Dgj_array')
    # actually change during the simulation, and the way they change is dictated by the meta-parameters.
    def MasterSim(self):
        # print('Does vm require grad? (before init)',self.ben.vm.requires_grad)
        # self.benHelp.InitializeBEN(self.ben,self.Inputs,self.Polarity_levels,self.num_polarity_levels,self.LearnPartial)
        # print('Does vm require grad? (after init) ',self.bfen.vm.requires_grad)
        if self.LearnFull or self.LearnPartial or self.LearnHomogenous:
            self.TotalWGrad = torch.zeros(self.NumLearnEdges)
            self.TotalBGrad = torch.zeros(self.NumLearnNodes)
        self.Simulate_nonlinear(self.NumSimIters[0],RecordVisualData=self.RecordVisualData)
        self.MSELoss(self.ben.vm_time[-self.NumEvalIters:,:,self.OutputCells],self.OutputVolts)
        self.ben.TotalError = self.ben.err.clone()
        if (self.LearnFull or self.LearnPartial or self.LearnHomogenous):  # TrackGrads=True
            self.Backprop()
        if self.ComputeSensitivity:
            # self.Sensitivity.ComputeGradients(self)
            self.Sensitivity.ComputeHigherGradients(self,cell=8)
            torch.set_grad_enabled(False)
            self.InitInputVm.requires_grad = False
            self.InitFullVm.requires_grad = False
        self.ben.vm_time_all = self.ben.vm_time.clone()
        if self.RecordVisualData:
            self.ben.cc_sig_time_all = self.ben.cc_sig_time.clone()
            self.ben.Dgj_time_all = self.ben.Dgj_time.clone()
        # PreOrderedIndices = [[1,2,3,0],[2,3,0,1],[3,0,1,2]]  # TESTING ONLY - for time series
        if self.SequentialInputs:
            idx = list(range(self.NumInputStates))  # initial ordering is lexicographic
            for s in range(self.InputSequenceLength-1):  # first piece of the sequence is already presented above
                idx = self.ShuffleIndices(idx)
                # idx = PreOrderedIndices[s]  # TESTING ONLY - for time series
                self.Inputs = [self.InputsOrig[i] for i in idx]
                self.Outputs = [self.OutputsOrig[i] for i in idx]
                self.benHelp.MapOutputVolts(self)
                self.Simulate_nonlinear(self.NumSimIters[0],RecordVisualData=self.RecordVisualData)
                self.MSELoss(self.ben.vm_time[-self.NumEvalIters:,:,self.OutputCells],self.OutputVolts)
                self.ben.TotalError = self.ben.TotalError + self.ben.err
                if (self.LearnFull or self.LearnPartial or self.LearnHomogenous):  # TrackGrads=True
                    self.Backprop()
                if self.ComputeSensitivity:
                    # self.Sensitivity.ComputeGradients(self)
                    self.Sensitivity.ComputeHigherGradients(self,cell=8)
                    torch.set_grad_enabled(False)
                    self.InitInputVm.requires_grad = False
                    self.InitFullVm.requires_grad = False
                self.ben.vm_time_all = torch.cat((self.ben.vm_time_all, self.ben.vm_time))
                if self.RecordVisualData:
                    self.ben.cc_sig_time_all = torch.cat((self.ben.cc_sig_time_all, self.ben.cc_sig_time))
                    self.ben.Dgj_time_all = torch.cat((self.ben.Dgj_time_all, self.ben.Dgj_time))
        self.ben.MeanError = self.ben.TotalError/self.InputSequenceLength

    def ComputeLearningRate(self):
        self.TotalWGrad.data[self.TotalWGrad.data!=self.TotalWGrad.data] = 0  # replace NaNs with 0
        self.TotalBGrad.data[self.TotalBGrad.data!=self.TotalBGrad.data] = 0
        self.BiasNan = (self.TotalBGrad.data == 0)
        if (self.BiasNan.any()):
            numNan = self.BiasNan.sum().item()
            bias_grad_noise = torch.normal(mean=0.0, std=torch.ones(numNan)*1e-5).view(1,-1)  # mainly to shake it from saturation
            self.TotalBGrad.data[self.BiasNan] = bias_grad_noise[0]
        # self.previous_delta_Bias[self.BiasNan] = 0
        self.WeightNan = (self.TotalWGrad.data == 0)
        if (self.WeightNan.any()):
            numNan = self.WeightNan.sum().item()
            weight_grad_noise = torch.normal(mean=0.0, std=torch.ones(numNan)*1e-5).view(1,-1)  # mainly to shake it from saturation
            self.TotalWGrad.data[self.WeightNan] = weight_grad_noise[0]
        # self.previous_delta_Weights[self.WeightNan] = 0
        self.weight_learning_rate = 10**((torch.max(np.floor(np.log10(np.abs(self.TotalWGrad.data))))*-1)+self.max_delta_exp_weight)  # max delta for weight
        self.bias_learning_rate = 10**((torch.max(np.floor(np.log10(np.abs(self.TotalBGrad.data))))*-1)+self.max_delta_exp_bias)  # max delta for bias; the '8' is to speed up a little

    def ComputeLearningModulator(self):
        self.modulator = 1/(1+np.exp(-self.slope_learn*(self.ben.TotalError.detach()-self.threshold_learn)))

    # def MasterLearn(self,DontSaveFlag):
    def MasterLearn(self):
        # Construct file name from arguments to save results
        Savefn = self.SaveFile.split('.')
        Savefn[0] += '_' + str(self.IndividualNum) + '_'
        Savefn[0] += self.SaveFileVersion
        SaveParamFile = '.'.join(Savefn)
        SaveParamFile = 'Data/' + SaveParamFile
        self.timeSinceResume = 0
        self.timeSinceRestart = 0
        StopSave = False
        for itn in range(self.NumIters):
            if (itn <= 100):
                self.alpha_momentum = 0.5
            else:
                self.alpha_momentum = 0.9
            self.MasterSim()
            if ((self.ben.TotalError.data > 999) or (np.isnan(self.ben.TotalError.data))):  # if this is true then the values have probably shoot up to infinity; simply restart in that case
                if self.LearnFull or self.LearnPartial or self.LearnHomogenous:
                    self.ben.Weights.data = torch.FloatTensor(np.random.uniform(-20,20,self.NumLearnEdges))
                    self.ben.Bias.data = torch.FloatTensor(np.random.uniform(0,1,self.NumLearnNodes))
                continue
            if ((self.LearnFull or self.LearnPartial or self.LearnHomogenous or self.LearnNone) & (itn % 1 == 0)):
                # print('individual = ',self.IndividualNum,' job = ',self.JobId,' iter = ',itn,' Error = ',self.ben.TotalError.data)
                self.AllErrors.append(np.round(self.ben.TotalError.item(),4))
                if (self.ben.TotalError.item() < self.BestError):
                    self.BestError = self.ben.TotalError.item()
                    self.BestErrors.append(self.BestError)
                    self.BestParams = [-99,itn,self.Layering,self.ben.InitWeights,self.ben.InitBias,self.ben.InitW,self.ben.Weights.data.clone(),self.ben.Bias.data.clone(),self.ben.W.data.clone(),self.AllErrors,self.BestErrors,self.JobId]
                self.BestParams[0] = itn
                currentTime = time.time()
                elapsedTime = currentTime - self.StartTime
                if ((((itn % 20) == 0) | (itn == (self.NumIters - 1))) and (not StopSave)):
                    torch.save(self.BestParams,SaveParamFile)
                if ((elapsedTime > np.floor(self.WallTimeThreshold)) and (not StopSave)):  # we don't want the job to abruptly end in the middle of a file-saving event
                    torch.save(self.BestParams,SaveParamFile)
                    StopSave = True
            if ((self.LearnFull or self.LearnPartial or self.LearnHomogenous) & (itn < (self.NumIters-1))):
                self.ComputeLearningRate()
                self.ComputeLearningModulator()
                # If all gradients are NaNs, restart search from the best parameter found so far
                if (self.WeightNan.all() & self.BiasNan.all()):
                    if (((self.timeSinceResume == 0) or (self.timeSinceResume > 100)) and (self.timeSinceRestart <= 500)):
                        # print('resuming search from the last best--------------')
                        if self.LearnPartial:
                            if (len(self.SimParamFiles) > 1):
                                self.benHelp.LoadMultipleParameterFiles(self.ben,self.NodeList,self.SimParamFiles,True)  # these files load data only for the rest of the network
                            else:
                                self.benHelp.LoadParameterFile(self.ben,self.NodeList,self.SimParamFiles[0],True) # these files load data only for the rest of the network
                            self.previous_delta_Weights = torch.zeros(self.NumLearnEdges)
                            self.previous_delta_Bias = torch.zeros(self.NumLearnNodes)
                        else:
                            self.benHelp.LoadParameterFile(self.ben,self.NodeList,SaveParamFile)
                            self.previous_delta_Weights = torch.zeros(self.NumLearnEdges)
                            self.previous_delta_Bias = torch.zeros(self.NumLearnNodes)
                        self.timeSinceResume = 1
                    elif (self.timeSinceRestart > 500):
                        # print('restarting search from scratch--------------')
                        if self.LearnFull or self.LearnPartial or self.LearnHomogenous:
                            self.ben.Weights.data = torch.FloatTensor(np.random.uniform(-1,1,self.NumLearnEdges))
                            self.ben.Bias.data = torch.FloatTensor(np.random.uniform(0,1,self.NumLearnNodes))
                            self.previous_delta_Weights = torch.zeros(self.NumLearnEdges)
                            self.previous_delta_Bias = torch.zeros(self.NumLearnNodes)
                        self.timeSinceRestart = 0
                        self.timeSinceResume = 1
                    if self.LearnFull or self.LearnPartial or self.LearnHomogenous:
                        delta_Weights = torch.normal(mean=0.0, std=torch.FloatTensor([0.1]*self.NumLearnEdges))
                        delta_Bias = torch.normal(mean=0.0, std=torch.FloatTensor([0.01]*self.NumLearnNodes))
                    self.timeSinceResume += 1
                    self.timeSinceRestart += 1
                else:
                    delta_Weights = -(self.modulator * self.weight_learning_rate * self.TotalWGrad.data)
                    delta_Bias = -(self.modulator * self.bias_learning_rate * self.TotalBGrad.data)
                    if (self.UseMomentum):
                        delta_Weights = (self.alpha_momentum * self.previous_delta_Weights) + ((1-self.alpha_momentum) * delta_Weights)  # momentum for Weights
                        self.previous_delta_Weights = delta_Weights
                        delta_Bias = (self.alpha_momentum * self.previous_delta_Bias) + ((1-self.alpha_momentum) * delta_Bias)   # momentum for Bias
                        self.previous_delta_Bias = delta_Bias
                self.ben.Weights.data += delta_Weights  # parameters (leaf variables) must be updated using the .data, else it'll be treated as an "in-place" operation and result in error
                self.ben.Bias.data += delta_Bias  # parameters (leaf variables) must be updated using the .data, else it'll be treated as an "in-place" operation and result in error
                # Weights bounding to [-1,1] not needed for the more nonlinear version of simulate (nothing wrong with bounding here, but tends to result in lower performance)
                # Weights.data[Weights.data > 1] = 1
                # Weights.data[Weights.data < -1] = -1
                # Bias restriction should NOT be done if weight normalization is NOT performed in simulate_nonlinear()
                self.ben.Bias.data[self.ben.Bias.data > 1] = 1
                # Bias.data[Bias.data < -1] = -1  # for the less nonlinear of simulate
                self.ben.Bias.data[self.ben.Bias.data < 0] = 0  # for the more nonlinear version of simulate; even if weight normalization is not performed (only Abs values of W are considered there)
        if ((self.LearnFull or self.LearnPartial or self.LearnHomogenous or self.LearnNone) and (self.IndividualNum != 999)):  # then it likely means that this backprop is part of a GA
            DoneParamFile = SaveParamFile + '.DONE'
            # os.mknod(DoneParamFile)  # a file to signal completion of this backpropagation to downstream processes
            open(DoneParamFile,'w').close()  # a file to signal completion of this backpropagation to downstream processes




