import sys
import ast
import numpy as np
from BioelectricNetwork.evolve.GA import GA
import torch
import time
import os
import glob

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd 
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# GA parameters
PopSize = int(args[16])
MutationRate = ast.literal_eval(args[17])
NumEpochs = int(args[18])  # NOTE: presently, this is not in use
# Storage parameters
SimParamFiles = args[19]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[20]
SaveFileVersion = args[21]
JobId = args[22]
RecordVisualData = ast.literal_eval(args[23])
WallTime = args[24]
epoch = int(args[25])

# Some useful variables derived from arguments
num_layers = len(Layering)
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)
# AND
Input_1 = [low,low,low]
Input_2 = [low,high,low]
Input_3 = [high,low,low]
Input_4 = [high,high,low]
# OR
Input_5 = [low,low,mid]
Input_6 = [low,high,mid]
Input_7 = [high,low,mid]
Input_8 = [high,high,mid]
# XOR
Input_9 = [low,low,high]
Input_10 = [low,high,high]
Input_11 = [high,low,high]
Input_12 = [high,high,high]
# All inputs
Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5,Input_6,Input_7,Input_8,Input_9,Input_10,Input_11,Input_12]
# AND
Output_1 = [low]
Output_2 = [low]
Output_3 = [low]
Output_4 = [high]
# OR
Output_5 = [low]
Output_6 = [high]
Output_7 = [high]
Output_8 = [high]
# XOR
Output_9 = [low]
Output_10 = [high]
Output_11 = [high]
Output_12 = [low]
# All outputs
Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5,Output_6,Output_7,Output_8,Output_9,Output_10,Output_11,Output_12]

InputCells = list(range(np.prod(Layering[0])))
NumCells = int(np.sum([np.prod(x) for x in Layering]))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))

# Other GA parameters
LatticeDims = Layering[1]
ConstructLatticeLayer = True
AllowIntraLyerConnections = True
InterLayerConnectionDensity = 0.5

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA
LoadNetwork = True  # because network is created by the GA module
NodeList = []
SequentialInputs = True
InputSequenceLength = 20
EvaluationProp = 0.1

def ClearFiles():
	Fnames = './Data/LogicGateEvolveMulti_*.dat'
	fnames = glob.glob(Fnames)
	for fname in fnames:
		if os.path.isfile(fname):
			os.remove(fname)
	fname = './Data/GAResults' + SaveFileVersion + '.dat'
	if os.path.isfile(fname):
		os.remove(fname)
	fname = './Data/BENArguments' + SaveFileVersion + '.dat'
	if os.path.isfile(fname):
		os.remove(fname)

def ClearBENFiles():
	for individual in range(PopSize):
		fname = './Data/LogicGateEvolveMulti_' + str(individual) + '_' + SaveFileVersion + '999.dat'
		if os.path.isfile(fname):
			os.remove(fname)

def SaveResults(Population,Scores):
	WeightGenotype, BiasGenotype = Population
	NewResults = [WeightGenotype, BiasGenotype, Scores]
	ResultsFile = './Data/GAResults' + SaveFileVersion + '.dat'
	if os.path.isfile(ResultsFile):
		Results = torch.load(ResultsFile)
	else:
		Results = []
	Results.append(NewResults)
	torch.save(Results,ResultsFile)

def RetrieveBackproppedPopulation(ga):
	Scores = torch.FloatTensor([-99]*PopSize)
	WeightGenotype = torch.FloatTensor(np.random.uniform(-1,1,(num_cells,num_cells)))
	WeightGenotype = WeightGenotype.repeat((1,PopSize))
	BiasGenotype = torch.FloatTensor(np.random.uniform(0,1,num_cells))
	BiasGenotype = BiasGenotype.repeat((PopSize))
	for individual in range(PopSize):
		fname = './Data/LogicGateEvolveMulti_' + str(individual) + '_' + SaveFileVersion + '.dat'
		try:
			_,_,_,_,_,_,BestWeights,BestBias,BestWeightMatrix,_,BestErrors,_ = torch.load(fname)
		except:
			print('File ', fname, ' does not exist.')
			continue  # NOTE: without 'continue', the loop will break by default
		else:
			# Update population with the best parameters obtained from backpropagation
			st1 = 0
			nd1 = num_cells
			st2 = individual*num_cells
			nd2 = st2 + num_cells
			BestW = BestWeightMatrix.clone()
			BestB = BestBias.clone()
			WeightGenotype[st1:nd1,st2:nd2] = BestW
			BiasGenotype[st2:nd2] = BestB
			Scores[individual] = BestErrors[-1]
	ga.Population = [WeightGenotype,BiasGenotype]
	return(Scores)

def GenerateBENArguments(Population):
	WeightGenotype, BiasGenotype = Population
	BENArguments = []
	for individual in range(PopSize):
		st1 = 0
		nd1 = num_cells
		st2 = individual*num_cells
		nd2 = st2 + num_cells
		W = WeightGenotype[st1:nd1,st2:nd2].clone()
		B = BiasGenotype[st2:nd2].clone() 
		NetworkParams = [W,B]
		LearnNodeList = []
		LearnEdgeList = []
		BlockNodes = []
		arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
		             NumSimIters,SimParamFiles,NodeList,\
		             LearnNodeList,LearnEdgeList,\
		             Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
		             dgj_scale,vm_scale,time_step,EvaluationProp,\
		             SaveFile,SaveFileVersion,JobId,\
		             RecordVisualData,\
		             Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
		             individual,StartTime,WallTime,\
		             BlockNodes]
		BENArguments.append(arguments)
	BENFile = './Data/BENArguments' + SaveFileVersion + '.dat'
	torch.save(BENArguments,BENFile)

if __name__ == '__main__':
	StartTime = time.time()
	ga_args = [PopSize,Layering,LatticeDims,MutationRate,ConstructLatticeLayer,AllowIntraLyerConnections,InterLayerConnectionDensity]
	ga = GA(ga_args)
	print('epoch = ',epoch)
	if epoch == 0:   # Generate population
		ClearFiles()
		ga.GeneratePopulation() 
	else:   # create new generation with new network structures and parameters: new layering, new AdjMatrix for the reservoir, new NetworkParams etc.
		Scores = RetrieveBackproppedPopulation(ga)  # load results file from the SimBEN processes
		SaveResults(ga.Population,Scores)
		ga.MicrobialTournament(Scores)
	GenerateBENArguments(ga.Population)  
	ClearBENFiles()


