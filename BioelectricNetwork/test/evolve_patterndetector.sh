walltime="0-00:10:00"
NumEpochs=2
PopSize=2
NumBackpropIters=2
PythonModule="EvolveBEN_PatternDetector_FrenchFlag"
SaveFilePrefix="PatternDetectEvolveFrench"
SaveFileVersion=999
SaveFilePrefix1=$SaveFilePrefix+"1.dat"
for (( epoch = 0; epoch <= $NumEpochs; epoch++ ))
do
	python -m BioelectricNetwork.evolve.$PythonModule $NumBackpropIters [600] LearnFull False True [[3,6],[5,5],[1,1]] False False [1] False 0.2 1.0 1.0 1.0 0.5 $PopSize 0.05 1 $SaveFilePrefix1 $SaveFilePrefix.dat $SaveFileVersion 999 False $walltime $epoch
	sleep 5
	python -m BioelectricNetwork.evolve.CheckFilesExist $PopSize $SaveFileVersion $SaveFilePrefix
	StartTime="$(date -u +%s)"
	AllDone="False"
	for (( individual = 1; individual <= $PopSize; individual++))
	do
		python -m BioelectricNetwork.evolve.RunSimBEN $individual $SaveFileVersion &
	done
	while [ $AllDone = "False" ]
	do
		PresentTime="$(date -u +%s)"
		ElapsedTime="$(($PresentTime-$StartTime))"
		AllDone=$(python -m BioelectricNetwork.evolve.CheckAllProcsComplete $PopSize $NumBackpropIters $SaveFileVersion $SaveFilePrefix $walltime $ElapsedTime)
	done
	unset AllDone
	sleep 5
done
