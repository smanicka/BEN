import sys
import ast
import torch
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import time
from mpl_toolkits import mplot3d
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import interpolate
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from matplotlib.collections import LineCollection

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]
GateId = int(args[22])
Net = args[23]
ComputePotentialSurface = ast.literal_eval(args[24])
NumSimItersPotential = ast.literal_eval(args[25])
ComputeTrajectories = ast.literal_eval(args[26])

AllGates = ['NOT','AND','OR','XOR','Multi','PatternReg']
Gate = AllGates[GateId]

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)

InputCells = list(range(np.prod(Layering[0])))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA, or when
LoadNetwork = True  # should be True for both simulation and learning
NodeList = []
SequentialInputs = False
InputSequenceLength = 1
EvaluationProp = 0.1
individual = 0
StartTime = time.time()

if LoadNetwork:
	data = torch.load('./Data/'+SimParamFiles[0])
	if (len(data)==12):
		_, _, _, InitWeights, InitBias, InitW, BestWeights, BestBias, BestW, _, _, _ = data  # Newest format has length 12
	else:
		print('Wrong file format')
	NetworkParams = [BestW.clone(),BestBias.clone()]
else:
	NetworkParams = []

TurnOffGapJuncGating = False
TurnOffSignalNonlinearity = False
BlockNodes = []
LearnNodeList = []
LearnEdgeList = []
BlockNodes = []

if Gate=='PatternReg':
	NumGridAxisPoints = num_polarity_levels
	# vm1 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
	# vm2 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(-1,1))
	# vm1 = vm1.repeat(NumGridAxisPoints,1)
	# vm2 = vm2.repeat(1,NumGridAxisPoints)
	vm1 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(-1,1))
	vm2 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
	vm1 = vm1.repeat(1,NumGridAxisPoints)
	vm2 = vm2.repeat(NumGridAxisPoints,1)
	Inputs = torch.cat((vm1.view(-1,1), vm2.view(-1,1)),dim=1)
	Inputs = Inputs.tolist()
	Outputs = Inputs  # NOTE: We actually don't know what the correct outputs should be here, but it doesn't matter anyway
	arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
				 NumSimItersPotential,SimParamFiles,NodeList,\
				 LearnNodeList,LearnEdgeList,\
				 Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
				 dgj_scale,vm_scale,time_step,EvaluationProp,\
				 SaveFile,SaveFileVersion,JobId,\
				 RecordVisualData,\
				 Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
				 individual,StartTime,WallTime,\
				 BlockNodes]
	simBEN = SimBEN(arguments)
	if ComputePotentialSurface:  # Compute potential energy surface
		simBEN.MasterLearn()
		InitVm = simBEN.ben.vm_time[0,:,:]
		FinalVm = simBEN.ben.vm_time[-1,:,:]
		VectorMagnitude = torch.sqrt(((FinalVm - InitVm)**2).sum(dim=1))
		# print(InitVm[num_polarity_levels,:],FinalVm[num_polarity_levels,:],VectorMagnitude)
		Potential = VectorMagnitude.view(NumGridAxisPoints,NumGridAxisPoints)
		torch.save(Potential,'Data/PotentialSurface.dat')
	else:
		Potential = torch.load('Data/PotentialSurface.dat')
	if ComputeTrajectories:
		NumGridAxisPoints = 5
		v1 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
		v2 = torch.tensor([mid],dtype=torch.int32)
		v1 = v1.repeat(1,1)
		v2 = v2.repeat(1,NumGridAxisPoints)
		Inputs = torch.cat((v1.view(-1,1), v2.view(-1,1)),dim=1)
		Inputs = Inputs.tolist()
		v2 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
		v1 = torch.tensor([mid],dtype=torch.int32)
		v2 = v2.repeat(1,1)
		v1 = v1.repeat(1,NumGridAxisPoints)
		Inputs2 = torch.cat((v1.view(-1,1), v2.view(-1,1)),dim=1)
		Inputs2 = Inputs2.tolist()
		Inputs.extend(Inputs2)
		v1 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
		v2 = torch.tensor([mid+int((low+mid)/2)],dtype=torch.int32)
		v1 = v1.repeat(1,1)
		v2 = v2.repeat(1,NumGridAxisPoints)
		Inputs3 = torch.cat((v1.view(-1,1), v2.view(-1,1)),dim=1)
		Inputs3 = Inputs3.tolist()
		Inputs.extend(Inputs3)
		v2 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
		v1 = torch.tensor([mid+int((low+mid)/2)],dtype=torch.int32)
		v2 = v2.repeat(1,1)
		v1 = v1.repeat(1,NumGridAxisPoints)
		Inputs4 = torch.cat((v1.view(-1,1), v2.view(-1,1)),dim=1)
		Inputs4 = Inputs4.tolist()
		Inputs.extend(Inputs4)
		v1 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
		v2 = torch.tensor([mid-int((low+mid)/2)],dtype=torch.int32)
		v1 = v1.repeat(1,1)
		v2 = v2.repeat(1,NumGridAxisPoints)
		Inputs5 = torch.cat((v1.view(-1,1), v2.view(-1,1)),dim=1)
		Inputs5 = Inputs5.tolist()
		Inputs.extend(Inputs5)
		v2 = torch.tensor(np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int).reshape(1,-1))
		v1 = torch.tensor([mid-int((low+mid)/2)],dtype=torch.int32)
		v2 = v2.repeat(1,1)
		v1 = v1.repeat(1,NumGridAxisPoints)
		Inputs6 = torch.cat((v1.view(-1,1), v2.view(-1,1)),dim=1)
		Inputs6 = Inputs6.tolist()
		Inputs.extend(Inputs6)
		Outputs = Inputs  # NOTE: We actually don't know what the correct outputs should be here, but it doesn't matter anyway
		arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
					 NumSimIters,SimParamFiles,NodeList,\
					 LearnNodeList,LearnEdgeList,\
					 Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
					 dgj_scale,vm_scale,time_step,EvaluationProp,\
					 SaveFile,SaveFileVersion,JobId,\
					 RecordVisualData,\
					 Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
					 individual,StartTime,WallTime,\
					 BlockNodes]
		simBEN = SimBEN(arguments)
		simBEN.MasterLearn()
		Trajectories = simBEN.ben.vm_time
		torch.save(Trajectories,'Data/Trajectories.dat')
	else:
		Trajectories = torch.load('Data/Trajectories.dat')

NumGridAxisPoints = num_polarity_levels
vm1sup = np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int)
vm2sup = np.linspace(0,num_polarity_levels-1,NumGridAxisPoints,dtype=np.int)
# vm1 = torch.tensor(vm1sup.reshape(1,-1))
# vm2 = torch.tensor(vm2sup.reshape(-1,1))
# vm1 = vm1.repeat(NumGridAxisPoints,1)
# vm2 = vm2.repeat(1,NumGridAxisPoints)
vm1 = torch.tensor(vm1sup.reshape(-1,1))
vm2 = torch.tensor(vm2sup.reshape(1,-1))
vm1 = vm1.repeat(1,NumGridAxisPoints)
vm2 = vm2.repeat(NumGridAxisPoints,1)
Inputs = torch.cat((vm1.view(-1,1), vm2.view(-1,1)),dim=1)
Inputs = Inputs.tolist()
InputVm = simBEN.benHelp.benSingleCell.InitVolts[Inputs,0]
InputVm1 = InputVm[:,0].view(NumGridAxisPoints,NumGridAxisPoints)
InputVm2 = InputVm[:,1].view(NumGridAxisPoints,NumGridAxisPoints)
# print(InputVm1,InputVm2)
InputVm1Sup = simBEN.benHelp.benSingleCell.InitVolts[vm1sup,0]
InputVm2Sup = simBEN.benHelp.benSingleCell.InitVolts[vm2sup,0]

VmMin,VmMax = InputVm1Sup.min(), InputVm1Sup.max()
Trajectories[Trajectories<VmMin] = VmMin
Trajectories[Trajectories>VmMax] = VmMax
fig = plt.figure()
ax = plt.axes(projection='3d')
# ax.plot_surface(InputVm1,InputVm2,Potential,cmap='coolwarm', edgecolor='none')
ax.set_xlabel('Vmem of cell 1 (mV)',fontsize=14,labelpad=10)
ax.set_ylabel('Vmem of cell 2 (mV)',fontsize=14,labelpad=10)
ax.set_zlabel('Potential energy (AU)',fontsize=14,labelpad=10)
ax.set_title('Energy landscape',fontsize=16)
tck = interpolate.bisplrep(InputVm1,InputVm2,Potential,s=0.01)
znew = interpolate.bisplev(InputVm1Sup, InputVm2Sup, tck)
ax.plot_surface(InputVm1,InputVm2,znew,cmap='copper_r', edgecolor='none',alpha=0.5)
tickLocations = np.arange(-0.08,0.1,0.02)
tickLocations = np.round(tickLocations,2)
ax.set_xticks(tickLocations)
ax.set_yticks(tickLocations)
tickLocations = np.round((tickLocations - 0.08)/2,2)
tickLabels = [str(loc*1000) for loc in tickLocations]
ax.set_xticklabels(tickLabels)
ax.set_yticklabels(tickLabels)
ax.tick_params(axis='both', which='major', labelsize=12, pad=5)
ax.grid(b=None)
initTimeStep = 0
for i in range(Trajectories.shape[1]):
	x = np.asarray(Trajectories[initTimeStep:,i,:].data)
	z = []
	zvprev = 0.0
	for j in range(x.shape[0]):
		v1,v2 = x[j,0],x[j,1]
		zv = interpolate.bisplev(v1, v2, tck)
		## NOTE: the below method of bispelv will cause an error, since for bisplev to work properly with array rather than
		# scalar arguments, they must be sorted in ascending order; for trajectory data such ordering may not be possible.
		# zv = interpolate.bisplev(InputVm1Sup, InputVm2Sup, tck)
		z.append(zv)
	t = np.arange(0,len(x[:,0]))
	points = np.array([x[:,0],x[:,1],z]).T.reshape(-1, 1, 3)
	segments = np.concatenate([points[:-1], points[1:]], axis=1)
	# lc = Line3DCollection(segments, cmap='Greys',norm=plt.Normalize(-0.7*max(t),max(t)))
	lc = Line3DCollection(segments, cmap='Greys',norm=mpl.colors.LogNorm(min(t)+0.1,max(t)))
	lc.set_array(t)
	ax.add_collection3d(lc)
	# ax.plot(x[:,0],x[:,1],z,color='black')
fig.tight_layout()
plt.show()





























