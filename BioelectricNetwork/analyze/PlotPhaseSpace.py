import sys
import ast
import torch
import numpy as np
from BioelectricNetwork.ben.SimBEN import SimBEN
import matplotlib.pyplot as plt
import time
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from matplotlib.collections import LineCollection
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib.transforms import Bbox

# Parse arguments
args = sys.argv
# Learning parameters
NumLearnIters = int(args[1])
NumSimIters = ast.literal_eval(args[2])
Mode = args[3]
Resume = ast.literal_eval(args[4])
UseMomentum = ast.literal_eval(args[5])  # True or False
# Network parameters
Layering = ast.literal_eval(args[6])
FullConnectivity = ast.literal_eval(args[7])  # FeedFwd
TopographicConnectivity = ast.literal_eval(args[8])  # Conv
LatticeLayers = ast.literal_eval(args[9])
# BEN system parameters
TurnOffGapJuncGating = ast.literal_eval(args[10])
DelayProp = ast.literal_eval(args[11])
ClampProp = ast.literal_eval(args[12])
dgj_scale = ast.literal_eval(args[13])
vm_scale = ast.literal_eval(args[14])
time_step = ast.literal_eval(args[15])
# Storage parameters
SimParamFiles = args[16]
SimParamFiles = SimParamFiles.replace('[','')
SimParamFiles = SimParamFiles.replace(']','')
SimParamFiles = SimParamFiles.split(',')
SaveFile = args[17]
SaveFileVersion = args[18]
JobId = args[19]
RecordVisualData = ast.literal_eval(args[20])
WallTime = args[21]
GateId = int(args[22])
Net = args[23]

AllGates = ['NOT','AND','OR','XOR','Multi','PatternReg']
Gate = AllGates[GateId]

# Some useful variables derived from arguments
cells_per_layer = [np.prod(x) for x in Layering]
num_cells = int(np.sum(cells_per_layer))
num_layers = len(Layering)

# Arguments specific to the experiment that won't vary (some of these could overlap with others, e.g., Polarity_levels)
num_polarity_levels = 21
Polarity_levels = []
Polarity_levels.extend([0.01])
Polarity_levels.extend(np.linspace(0.05,0.95,num_polarity_levels-2))
Polarity_levels.extend([0.99])
Polarity_levels = np.array(Polarity_levels)
low = 0
high = num_polarity_levels - 1
mid = int((high + low)/2)

if ((Gate=='AND') | (Gate=='OR') | (Gate=='XOR')):
	Input_1 = [low,low]
	Input_2 = [low,high]
	Input_3 = [high,low]
	Input_4 = [high,high]
	Input_5 = [mid,mid]
	# Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5]
	Inputs = [Input_1,Input_2,Input_3,Input_4]
	# Inputs = [Input_4,Input_1,Input_2,Input_3]
	if Gate=='AND':
		Output_1 = [low]
		Output_2 = [low]
		Output_3 = [low]
		Output_4 = [high]
		Output_5 = [mid]
	elif Gate=='OR':
		Output_1 = [low]
		Output_2 = [high]
		Output_3 = [high]
		Output_4 = [high]
	elif Gate=='XOR':
		Output_1 = [low]
		Output_2 = [high]
		Output_3 = [high]
		Output_4 = [low]
	Outputs = [Output_1,Output_2,Output_3,Output_4]
	# Outputs = [Output_4,Output_1,Output_2,Output_3]
	# Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5]
elif Gate=='NOT':
	Input_1 = [low]
	Input_2 = [high]
	Input_3 = [mid]
	# Input_2 = [mid+10]   # TESTING ONLY
	# Inputs = [Input_1,Input_2,Input_3]
	Inputs = [Input_1,Input_2]
	Output_1 = [high]
	Output_2 = [low]
	Output_3 = [mid]
	# Outputs = [Output_1,Output_2,Output_3]
	Outputs = [Output_1,Output_2]
elif Gate=='Multi':
	# OR
	Input_1 = [low,low,low]
	Input_2 = [low,high,low]
	Input_3 = [high,low,low]
	Input_4 = [high,high,low]
	# AND
	Input_5 = [low,low,high]
	Input_6 = [low,high,high]
	Input_7 = [high,low,high]
	Input_8 = [high,high,high]
	Inputs = [Input_1,Input_2,Input_3,Input_4,Input_5,Input_6,Input_7,Input_8]
	# OR
	Output_1 = [low]
	Output_2 = [low]
	Output_3 = [low]
	Output_4 = [high]
	# AND
	Output_5 = [low]
	Output_6 = [high]
	Output_7 = [high]
	Output_8 = [high]
	Outputs = [Output_1,Output_2,Output_3,Output_4,Output_5,Output_6,Output_7,Output_8]
elif Gate=='PatternReg':
	Num_samples = 20
	def PerturbPattern(pattern,delta):
		num_pixels = len(pattern)
		delta_index_range = np.arange(-delta,delta+1)
		perturbation = np.random.choice(delta_index_range,num_pixels)
		pattern = (pattern + perturbation)
		pattern[pattern < 0] = 0
		pattern[pattern >= num_polarity_levels] = num_polarity_levels-1
		return(list(pattern))  # turn np array into a regular list
	Main_pattern_1 = [high,low]
	Main_pattern_2 = [high,high]
	Inputs = list([Main_pattern_1,Main_pattern_2])  # Note that "Inputs" contains indices into "Polarity_levels", not the polarities themselves
	Outputs = list([Main_pattern_1,Main_pattern_2])  # first item corresponds to the main pattern
	Num_samples_1 = Num_samples_2 = int(Num_samples/2)
	MaxPerturbSize = int(num_polarity_levels/2) - 4
	for i in range(Num_samples_1 - 1):  # Note the main pattern, not included here, is a correct sample
		Inputs.append(PerturbPattern(Main_pattern_1,MaxPerturbSize))
		Outputs.append(Main_pattern_1)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list
	for i in range(Num_samples_2 - 1):
		Inputs.append(PerturbPattern(Main_pattern_2,MaxPerturbSize))
		Outputs.append(Main_pattern_2)  # NOTE: It's easy to oversee this, but it's essential to have individual outputs in a list

InputCells = list(range(np.prod(Layering[0])))
OutputCells = list(range(sum(cells_per_layer[0:(num_layers-1)]),num_cells))
# OutputCells = InputCells.copy()
# NumCells = int(np.sum([np.prod(x) for x in Layering]))
# OutputCells.extend(list(range(NumCells-1,NumCells)))

# Network parameters
CreateNetwork = False  # true when there is no interaction with GA, or when
LoadNetwork = True  # should be True for both simulation and learning
NodeList = []
SequentialInputs = True
InputSequenceLength = 4
EvaluationProp = 0.1
individual = 0
StartTime = time.time()

if LoadNetwork:
	data = torch.load('./Data/'+SimParamFiles[0])
	if (len(data)==12):
		_, _, _, InitWeights, InitBias, InitW, BestWeights, BestBias, BestW, _, _, _ = data  # Newest format has length 12
	else:
		print('Wrong file format')
	NetworkParams = [BestW.clone(),BestBias.clone()]
else:
	NetworkParams = []

TurnOffGapJuncGating = False
TurnOffSignalNonlinearity = False
BlockNodes = []
LearnNodeList = []
LearnEdgeList = []
BlockNodes = []

arguments = [NumLearnIters,Mode,CreateNetwork,LoadNetwork,Resume,UseMomentum,\
			 NumSimIters,SimParamFiles,NodeList,\
			 LearnNodeList,LearnEdgeList,\
			 Layering,FullConnectivity,TopographicConnectivity,LatticeLayers,TurnOffGapJuncGating,DelayProp,ClampProp,NetworkParams,\
			 dgj_scale,vm_scale,time_step,EvaluationProp,\
			 SaveFile,SaveFileVersion,JobId,\
			 RecordVisualData,\
			 Polarity_levels,Inputs,Outputs,SequentialInputs,InputSequenceLength,InputCells,OutputCells,\
			 individual,StartTime,WallTime,\
			 BlockNodes]

simBEN = SimBEN(arguments)

simBEN.MasterLearn()

print(simBEN.ben.MeanError)

vm_time = simBEN.ben.vm_time_all
cc_sig_time = simBEN.ben.cc_sig_time_all
W_time = simBEN.ben.Dgj_time
NumEdges = simBEN.ben.NumEdges
EdgeList = np.array(torch.nonzero(torch.triu(simBEN.ben.BaseAdj)))
n = simBEN.ben.n
NumInputStates = vm_time.shape[1]

state_time = vm_time*1000
# state_time = (state_time/2)-40  # rescale Vmem so that max is around 0 mV
# state_time = cc_sig_time

sAll = []
for i in range(NumInputStates):
# for i in [2]:
	sAll.append(state_time[:,i,:].numpy())

Plot = True
PlotSplitSequence = True

fig = plt.figure()
if n > 2:
	ax = plt.axes(projection="3d")
else:
	ax = plt.axes()

if Plot:
	for i in range(len(sAll)):
		s = sAll[i]
		if PlotSplitSequence:
			for seq in range(InputSequenceLength):
				c = 'Greens'
				st = int(seq*NumSimIters[0])
				nd = int(st + NumSimIters[0])
				if n > 2:  # typically this means two inputs
					x = s[st:nd,0]
					y = s[st:nd,1]
					z = s[st:nd,n-1]
					# if n > 3:
					# 	z = s[st:nd,4]
					# else:
					# 	z = s[st:nd,3]
					# print(x[-1],y[-1],z[-1])
					t = np.arange(0,len(x))
					points = np.array([x,y,z]).T.reshape(-1, 1, 3)
					segments = np.concatenate([points[:-1], points[1:]], axis=1)
					lc = Line3DCollection(segments, cmap=c,norm=plt.Normalize(-0.3*max(t),max(t)))
					lc.set_array(t)
					ax.add_collection3d(lc)
				else:    # typically this means just one input
					x = s[:,0]
					z = s[:,n-1]
					t = np.arange(0,len(x))
					points = np.array([x,z]).T.reshape(-1, 1, 2)
					segments = np.concatenate([points[:-1], points[1:]], axis=1)
					lc = LineCollection(segments, cmap=c,norm=plt.Normalize(-0.6*max(t),max(t)))
					lc.set_array(t)
					ax.add_collection(lc)
		else:
			c = 'copper'
			x = s[:,0]
			y = s[:,1]
			z = s[:,n-1]
			t = np.arange(0,len(x))
			points = np.array([x,y,z]).T.reshape(-1, 1, 3)
			segments = np.concatenate([points[:-1], points[1:]], axis=1)
			lc = Line3DCollection(segments, cmap=c,norm=plt.Normalize(-0.3*max(t),max(t)))
			lc.set_array(t)
			ax.add_collection3d(lc)

	if n > 2:
		ax.set_xlim(-100,100)
		ax.set_ylim(-100,100)
		ax.set_zlim(-100,100)
		ax.set_xlabel('Input1 Vmem (mV)',fontsize=16)
		ax.set_ylabel('Input2 Vmem (mV)',fontsize=16)
		ax.set_zlabel('Output Vmem (mV)',fontsize=16)
		ax.set_xticks(np.array([-100,-50,0,50,100]))
		ax.set_yticks(np.array([-100,-50,0,50,100]))
		ax.set_zticks(np.array([-100,-50,0,50,100]))
		ax.tick_params(axis='both',labelsize=12)
		ax.xaxis.labelpad=10
		ax.yaxis.labelpad=10
		ax.zaxis.labelpad=10
	else:
		ax.set_xlim(-100,10)
		ax.set_ylim(-100,10)
		ax.set_xlabel('Input1 Vmem (mV)',fontsize=30)
		ax.set_ylabel('Output Vmem (mV)',fontsize=30)
		ax.tick_params(axis='both',labelsize=24)
		ax.xaxis.labelpad=5
		ax.yaxis.labelpad=5

	ax.grid(True)
	fig.tight_layout()
	plt.show()
	fname = './Figures/PhaseSpaceVmem' + Gate + str(Net) + '.png'
	# fname = './Figures/PhaseSpaceVmemPatternRegulator.png'
	fig.savefig(fname,bbox_inches='tight',pad_inches=0.2)





























