#!/bin/bash
walltime="1-00:00:00"
cutofftime="0-05:00:00"
NumEpochs=1000000
PopSize=100
NumBackpropIters=1
PythonModule=$1
SaveFilePrefix=$2
SaveFileVersion=$3
Seed=$4
SeedFile=$5
SeedNoise=$6
SaveFilePrefix1=$SaveFilePrefix+"1.dat"
for (( epoch = 0; epoch <= $NumEpochs; epoch++ ))
do
  python -m BioelectricNetwork.evolve.$PythonModule $NumBackpropIters [600] LearnNone False True [[2,1],[2,1],[1,1]] False False [] False 0.0 1.0 1.0 1.0 1.0 $PopSize 0.07 0.05 0.05 0.01 0.01 $SaveFilePrefix1 $SaveFilePrefix.dat $SaveFileVersion 999 False $Seed $SeedFile $SeedNoise $walltime $epoch
	sleep 5
	python -m BioelectricNetwork.evolve.CheckFilesExist $PopSize $3 $2
	StartTime="$(date -u +%s)"
	for (( individual = 1; individual <= $PopSize; individual++))
	do
		sbatch --time=$walltime --mem=1G --output=$PythonModule$individual.out --error=$PythonModule$individual.err --job-name=$SaveFilePrefix --export=individual=$individual,SaveFileVersion=$SaveFileVersion evolve_logicgate_RunSimBEN.sbatch
	done
  PresentTime="$(date -u +%s)"
  ElapsedTime="$(($PresentTime-$StartTime))"
  python -m BioelectricNetwork.evolve.CheckAllProcsComplete $PopSize $NumBackpropIters $SaveFileVersion $SaveFilePrefix $cutofftime $ElapsedTime
	sleep 5
done
