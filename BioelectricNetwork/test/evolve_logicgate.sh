#!/bin/bash
walltime="0-10:00:00"
cutofftime="0-05:00:00"
NumEpochs=2
PopSize=4
NumBackpropIters=1
PythonModule="EvolveBEN_LogicGate_AND_small"
SaveFilePrefix="LogicGateEvolveANDSmall"
SaveFileVersion=999
Sfx="1.dat"
SaveFilePrefix1=$SaveFilePrefix$Sfx
Seed="True"
SeedFile="LogicGateAND_0_320.dat"
SeedNoise="0.01"
for (( epoch = 0; epoch <= $NumEpochs; epoch++ ))
do
  python -m BioelectricNetwork.evolve.$PythonModule $NumBackpropIters [600] LearnNone False True [[2,1],[2,1],[1,1]] False False [] False 0.0 1.0 1.0 1.0 1.0 $PopSize 0.07 0.05 0.05 0.01 0.01 $SaveFilePrefix1 $SaveFilePrefix.dat $SaveFileVersion 999 False $Seed $SeedFile $SeedNoise $walltime $epoch
	sleep 5
	python -m BioelectricNetwork.evolve.CheckFilesExist $PopSize $SaveFileVersion $SaveFilePrefix
	StartTime="$(date -u +%s)"
	for (( individual = 1; individual <= $PopSize; individual++))
	do
		python -m BioelectricNetwork.evolve.EvolveBEN_RunSimBEN $individual $SaveFileVersion 999 &
	done
  PresentTime="$(date -u +%s)"
  ElapsedTime="$(($PresentTime-$StartTime))"
  python -m BioelectricNetwork.evolve.CheckAllProcsComplete $PopSize $NumBackpropIters $SaveFileVersion $SaveFilePrefix $cutofftime $ElapsedTime
	sleep 5
done